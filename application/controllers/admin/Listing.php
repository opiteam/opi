<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Listing extends CI_Controller {

	function index() {
        $this->load->library("pagination");
        $config = array();
        $config["base_url"] = base_url('admin/listing/index');
        $config["total_rows"] = $this->AdminModel->categoriesCount();
        $config["per_page"] = 15;
        $config["uri_segment"] = 3;
        $config['last_link'] = 'Last';
        $config['first_link'] = 'First';
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$data = array(
			'query' => $this->AdminModel->categories($config["per_page"], $page),
			'title' => "Category List",
            'link' => $this->pagination->create_links()
			);
		$this->load->view('admin/items', $data);
	}

	function addCategory() {
		
        $this->load->library('form_validation');
        $this->load->helper('security');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');        
        $this->form_validation->set_rules('name', 'name', 'trim|required|strip_tags|xss_clean');


        if ($this->form_validation->run() === FALSE)
            {       

                $this->load->view('admin/addCategory');
                        
            }
        else
            {
            $config['upload_path'] = './uploads/categories/';
            $config['allowed_types'] = 'doc|docx|pdf|txt|text|rtf|jpg|gif|png'; 
            $config['max_size'] = '1024';

            $this->load->library('upload', $config);

            if (!$this->upload->do_upload()) {   
                if ("You did not choose image for upload." != $this->upload->display_errors('','')) {  
                    $error = array('error' => $this->upload->display_errors());
                    $this->load->view('admin/addCategory', $error);
                }  
                else {  
                
                $this->AdminModel->addCategory();
                redirect('admin/listing');
                }  
            }  
            else {  
                //Image Resizing
                $config['source_image'] = $this->upload->upload_path.$this->upload->file_name;
                $config['maintain_ratio'] = FALSE;
                $config['width'] = 48;
                $config['height'] = 48;

                $this->load->library('image_lib', $config);
                $this->image_lib->resize();
                $data = array('upload_data' => $this->upload->data()
                            );
                $data1 = $this->upload->data();
                $data2 = $data1['file_name'];
                $this->AdminModel->addCategory($data2);
                $id = $this->mysql->insert_id();
                $this->session->set_flashdata('notification', 'Category Added');
                redirect('admin/listing/editCategory/'.$id);
            }
        }
	}

	function addItem() {

        $config['ul_class']='treeview';
        $config['table']='kategorije';
        $this->load->library('tree',$config);
        //$tree = $this->tree->getTree();
		
		$data = array(
			'podkateg' => $this->AdminModel->subcategoryList(),
			'title' => "Dodaj firmu",
            'tree' => $this->tree->getTree()
			);
    
      
		$this->load->library('form_validation');
        $this->load->helper('security');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');      
        $this->form_validation->set_rules('ime_firme', 'ime_firme', 'trim|required|strip_tags|xss_clean');
        

        if ($this->form_validation->run() === FALSE)
            {   
                $this->load->view('admin/addItem', $data);
                        
            }
        else
            {
            
                
                $config['upload_path'] = './uploads/';
                $config['allowed_types'] = 'gif|jpg|png|jpeg|';
                $config['max_size']    = '1000';
                $config['max_width']  = '1600';
                $config['max_height']  = '1200';
        
                $this->load->library('upload', $config);

                $this->upload->do_multi_upload('userfile');

                $image = $this->upload->get_multi_upload_data('userfile');

                $this->load->library('image_lib', $config);

                foreach ($image as $img) {

                $config['image_library'] = 'gd2';
                $config['source_image'] = './uploads/'.$img['file_name'];
                $config['new_image'] = './uploads/thumbs/'.$img['file_name'];
                $config['maintain_ratio'] = TRUE;
                $config['width']     = 270;
                $config['height']     = 270;

                $this->image_lib->initialize($config);
 
                $this->image_lib->resize();
                $this->image_lib->clear();

                }   

        
                $this->AdminModel->addItem();
                $id = mysql_insert_id();
                $this->AdminModel->itemsCategories($id);
                $slike = $this->upload->get_multi_upload_data();
                $this->AdminModel->imagesItems($id, $slike);
                $this->session->set_flashdata('notification', 'Uspješno dodana firma');
                redirect('admin/firme/uredi-firmu/'.$id);
            
            }
	   }

	function category($id) {
        $this->load->library("pagination");
        $config = array();
        $config["base_url"] = base_url('admin/listing/category/'.$id);
        $config["total_rows"] = $this->AdminModel->subcategoriesCount($id);
        $config["per_page"] = 15;
        $config["uri_segment"] = 5;
        $config['last_link'] = 'Last';
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(5)) ? $this->uri->segment(5) : 0;
		$data = array(
                'query' => $this->AdminModel->subcategories($id, $config["per_page"], $page),
                'cat' => $this->AdminModel->category($id),
                'link' => $this->pagination->create_links(),
                );
        foreach ($data['cat'] as $category) {
            $data['title'] = $category->categoryName;
        }


		$this->load->view('admin/category', $data);
	}

	function addSubcategory() {
		$data = array(
			'title' => "Dodaj podkategoriju"
			);
		$this->load->library('form_validation');
        $this->load->helper('security');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');        
        $this->form_validation->set_rules('ime_podkategorije', 'ime_podkategorije', 'trim|required|strip_tags|xss_clean');


        if ($this->form_validation->run() === FALSE)
            {       
                $this->load->view('admin/addSubcategory', $data);
                        
            }
        else
            {

                $this->AdminModel->addSubcategory();
                $id = mysql_insert_id();
                redirect('admin/firme/uredi-podkategoriju/'.$id);
                
            }
	}

	function subcategories() {
		$data = array(
			'kategorije' => $this->AdminModel->categories(),
			'title' => "Lista Firmi"
			);

		$this->load->view('admin/firms', $data);
	}

	function subcategory($id) {
        $this->load->library("pagination");
        $config["base_url"] = base_url('admin/listing/subcategory/'.$id);
        $config["total_rows"] = $this->AdminModel->itemsCount($id);
        $config["per_page"] = 15;
        $config["uri_segment"] = 5;
        $config['last_link'] = 'Last';
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(5)) ? $this->uri->segment(5) : 0;
		$data = array(
                'query' => $this->AdminModel->getListings($id, $config["per_page"], $page),
                'podkat' => $this->AdminModel->subcategory($id),
                'title' => "Listings",
                'link' => $this->pagination->create_links(),
                );

		$this->load->view('admin/subcategory', $data);
	}

    function allItems() {
        $this->load->library("pagination");
        $config["base_url"] = base_url() . "admin/sve_firme";
        $config["total_rows"] = $this->AdminModel->ukupno_firmi();
        $config["per_page"] = 15;
        $config["uri_segment"] = 3;
        $config['last_link'] = 'Zadnja';
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $id = '';
        $data = array(
                'query' => $this->AdminModel->allItems($config["per_page"], $page),
                'podkat' => $this->AdminModel->subcategory($id),
                'title' => "Sve firme",
                'link' => $this->pagination->create_links(),
                );

        $this->load->view('admin/podkategorija', $data);
    }

	function editItem($id) {
        $this->session->set_userdata('last_page', current_url());
        $config['ul_class']='treeview';
        $config['table']='kategorije';
        $this->load->library('tree',$config);

		$data = array(
			'title' => "Uredi Firmu",
			'que' => $this->AdminModel->item($id),
            'podkateg' => $this->AdminModel->currentSubcategory($id),
			'slika' => $this->AdminModel->image($id),
            'tree' => $this->tree->getTree(),
            'button_text' => 'Uredi'
			);
		$this->load->library('form_validation');
        $this->load->helper('security');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');     
        $this->form_validation->set_rules('ime_firme', 'ime_firme', 'trim|required|strip_tags|xss_clean');

        if ($this->form_validation->run() === FALSE)
            {       
                $this->load->view('admin/editItem', $data);
                        
            }
        else
            {
                $config['upload_path'] = './uploads/';
                $config['allowed_types'] = 'gif|jpg|png|jpeg|';
                $config['max_size']    = '1000';
                $config['max_width']  = '1600';
                $config['max_height']  = '1200';
        
                $this->load->library('upload', $config);

                $this->upload->do_multi_upload('userfile');

                $image = $this->upload->get_multi_upload_data('userfile');

                $this->load->library('image_lib', $config);

                foreach ($image as $img) {

                $config['image_library'] = 'gd2';
                $config['source_image'] = './uploads/'.$img['file_name'];
                $config['new_image'] = './uploads/thumbs/'.$img['file_name'];
                $config['maintain_ratio'] = TRUE;
                $config['width']     = 270;

                $this->image_lib->initialize($config);
 
                $this->image_lib->resize();
                $this->image_lib->clear();

                }   

                $id = $this->input->post('id');
                $this->session->set_flashdata('id', $id);
                $this->AdminModel->editItem($id);
                $this->AdminModel->editItemsCategories($id);
                $slike = $this->upload->get_multi_upload_data();
                $this->AdminModel->imagesItems($id, $slike);
                $this->session->set_flashdata('notification', 'Firma uspješno uređena');
                redirect($this->session->userdata('last_page'));
            }
	}

    function approveItem($id) {

        $config['ul_class']='treeview';
        $config['table']='kategorije';
        $this->load->library('tree',$config);

        $data = array(
            'title' => "Odobri Firmu",
            'que' => $this->AdminModel->item($id),
            'podkateg' => $this->AdminModel->currentSubcategory($id),
            'slika' => $this->AdminModel->image($id),
            'tree' => $this->tree->getTree(),
            'button_text' => 'Odobri'
            );
        $this->load->library('form_validation');
        $this->load->helper('security');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');     
        $this->form_validation->set_rules('ime_firme', 'ime_firme', 'trim|required|strip_tags|xss_clean');

        if ($this->form_validation->run() === FALSE)
            {       
                $this->load->view('admin/editItem', $data);
                        
            }
        else
            {
                $config['upload_path'] = './uploads/';
                $config['allowed_types'] = 'gif|jpg|png|jpeg|';
                $config['max_size']    = '1000';
                $config['max_width']  = '1600';
                $config['max_height']  = '1200';
        
                $this->load->library('upload', $config);

                $this->upload->do_multi_upload('userfile');

                $image = $this->upload->get_multi_upload_data('userfile');

                $this->load->library('image_lib', $config);

                foreach ($image as $img) {

                $config['image_library'] = 'gd2';
                $config['source_image'] = './uploads/'.$img['file_name'];
                $config['new_image'] = './uploads/thumbs/'.$img['file_name'];
                $config['maintain_ratio'] = TRUE;
                $config['width']     = 270;

                $this->image_lib->initialize($config);
 
                $this->image_lib->resize();
                $this->image_lib->clear();

                }   

                $id = $this->input->post('id');
                $this->session->set_flashdata('id', $id);
                $this->AdminModel->approveItem($id);
                $this->AdminModel->editItemsCategories($id);
                $slike = $this->upload->get_multi_upload_data();
                $this->AdminModel->imagesItems($id, $slike);
                $this->load->helper('url');
                $link_firme = url_title(convert_accented_characters($this->input->post('ime_firme')), 'dash', TRUE);
                $this->load->library('email');
                $config['wordwrap'] = TRUE;
                $config['mailtype'] = 'html';
                $this->email->initialize($config);
                $this->email->from('info@opi.ba', 'Opi.ba Poslovni imenik');
                $this->email->to($this->input->post('email1'));  

                $this->email->subject('item odobrena');
                $this->email->message('Poštovani,<br>

                    Your request for adding new listing in our database is successfully approved. <br>

                    You can check your listing <a href="'.base_url().'poslovni-imenik/firma/'.$id.'/'.$link_firme.'">>>>here<<<</a><br>

                    Thank you for using '.$site_name.'
                    <br>
                    <br>

                    S poštovanjem
                    <br>
                    '.$site_name.' team'

                    );  

                $this->email->send();
                $this->session->set_flashdata('notification', 'Listing successfully approved');
                redirect('admin/firme/uredi-firmu/'.$id);
            }
    }

    function editCategory($id) {
        $data = array(
            'query' => $this->AdminModel->category($id)
            );

        $this->load->library('form_validation');
        $this->load->helper('security');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');        
        $this->form_validation->set_rules('name', 'name', 'trim|required|strip_tags|xss_clean');


        if ($this->form_validation->run() === FALSE)
            {       
                $this->load->view('admin/editCategory', $data);
                        
            }
        else
            {
                $this->AdminModel->editCategory();
                redirect('admin/listing');
            }
    }

    function editSubcategory($id) {
        $data = array(
            'query' => $this->AdminModel->subcategory($id)
            );

        $this->load->library('form_validation');
        $this->load->helper('security');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');        
        $this->form_validation->set_rules('name', 'name', 'trim|required|strip_tags|xss_clean');


        if ($this->form_validation->run() === FALSE)
            {       
                $this->load->view('admin/editSubcategory', $data);
                        
            }
        else
            {
                $this->AdminModel->editSubcategory();
                redirect('admin/kategorije');
            }
    }

	function removeCategory($id) {
		if ($this->AdminModel->removeCategory($id))
   		{
      		$status = 'success';
      		$msg = 'Category successfully deleted';
   		}
  	 	else
  		{
      		$status = 'error';
      		$msg = 'Greška pri brisanju kategorije';
   		}
   			echo json_encode(array('status' => $status, 'msg' => $msg));
	}

    function removeSubcategory($id) {
        if ($this->AdminModel->removeSubcategory($id) && $this->AdminModel->removeItemsCategories($id))
        {
            $status = 'success';
            $msg = 'Subcategory successfully deleted';
        }
        else
        {
            $status = 'error';
            $msg = 'error';
        }
            echo json_encode(array('status' => $status, 'msg' => $msg));
    }

	function removeImage($id) {
		if ($this->AdminModel->removeImage($id))
   		{
      		$status = 'success';
      		$msg = 'Image successfully deleted';
   		}
  	 	else
  		{
      		$status = 'error';
      		$msg = 'Error';
   		}
   			echo json_encode(array('status' => $status, 'msg' => $msg));
	}

    function removeItem($id) {
        if ($this->AdminModel->removeItem($id))
        {
            $status = 'success';
            $msg = 'Listing successfully deleted';
        }
        else
        {
            $status = 'error';
            $msg = 'Error';
        }
            echo json_encode(array('status' => $status, 'msg' => $msg));
    }
}