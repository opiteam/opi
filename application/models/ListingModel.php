<?php
class ListingModel extends CI_Model {
    
    function categories() {
    $this->db->order_by('categoryName', 'asc');
    $query = $this->db->get('categories');
    return $query->result();
  }

  function subcategories($id) {
    $this->db->where('parentId', $id);
    $query = $this->db->get('categories');
    return $query->result();
  }

  function ItemNumberPerCategory($id) {
    $this->db->select();
    $this->db->from('items_categories');
    $this->db->join('categories', 'categories.id = items_categories.categoryId');
    $this->db->where('parentId', $id);
    $query = $this->db->get();
    return $query->num_rows();
  }

  function ItemNumberPerSubcategory($id) {
    $this->db->select();
    $this->db->from('items_categories');
    $this->db->join('categories', 'categories.id = items_categories.categoryId');
    $this->db->where('items_categories.categoryId', $id);
    $query = $this->db->get();
    return $query->num_rows();
  }

  function category($link) {
    $this->db->where('categorySlug', $link);
    $q = $this->db->get('categories');
    $lin = $q->row_array();

    $this->db->where('parentId', $lin['id']);
    $query = $this->db->get('categories');
    return $query->result();
  }

  function CategoryName($link) {
    $this->db->where('categorySlug', $link);
    $query = $this->db->get('categories');
    return $query->row_array();
  }

  function SubcategoryName($link) {
    $this->db->where('categorySlug', $link);
    $query = $this->db->get('categories');
    return $query->row_array();
  }

  function SubcategoryCount($link) {
    $this->db->select('categories.categorySlug, listings.name, listings.description, listings.id, listings.slug');
    $this->db->from('listings');
    $this->db->join('items_categories', 'items_categories.item_id = listings.id');
    $this->db->join('categories', 'categories.id = items_categories.categoryId', 'right');
    $this->db->where('categories.categorySlug', $link);
    $this->db->where('listings.approved', 1);
    $query = $this->db->get();
    return $query->num_rows(); 
  }

  function subcategory($link, $limit, $start) {
    $this->db->limit($limit, $start);
    $this->db->select('listings.Ime, listings.description, listings.id, listings.slug');
    $this->db->from('listings');
    $this->db->join('items_categories', 'items_categories.listingId = listings.id');
    $this->db->join('categories', 'categories.id = items_categories.categoryId', 'right');
    $this->db->where('categories.categorySlug', $link);
    $this->db->where('listings.approved', 1);
    $this->db->order_by('date', 'desc');
    $query = $this->db->get();
    return $query->result();
  }

  function listing($link) {
    $this->db->where('slug', $link);
    $this->db->where('approved', 1);
    $query = $this->db->get('listings');
    return $query->row_array();
  }

  function images($link) {
    $this->db->join('listings', 'images.listingId = listings.id');
    $this->db->where('slug', $link);
    $query = $this->db->get('images');
    return $query->result();
  }

  function ImagesList($id) {
    $this->db->select();
    $this->db->from('images');
    $this->db->join('listings', 'images.listingId = listings.id');
    $this->db->where('listingId', $id);
    $query = $this->db->get();
    return $query->result();
  }
  
  function ListingsRecomended() {
    $this->db->limit(5);
    $this->db->select('categories.parentId, categories.categorySlug, listings.slug, listings.name, listings.id, categories.categoryName');
    $this->db->from('listings');
    $this->db->join('items_categories', 'items_categories.item_id = listings.id');
    $this->db->join('categories', 'categories.id = items_categories.categoryId');
    $this->db->where("listings.featured", 1);
    $this->db->order_by("listings.name", "random");
    $query = $this->db->get();
    return $query->result();
  }

  function RecomendedListingsHome() {
    $this->db->limit(8);
    $this->db->select('categories.parentId, categories.categorySlug, listings.slug, listings.name, listings.id, categories.categoryName');
    $this->db->from('listings');
    $this->db->join('items_categories', 'items_categories.listingId = listings.id');
    $this->db->join('categories', 'categories.id = items_categories.categoryId');
    $this->db->where("listings.featured", 1);
    $this->db->order_by("listings.name", "random");
    $query = $this->db->get();
    return $query->result();
  }

  function NewListings() {
    $this->db->limit(5);
    $this->db->select('categories.parentId, categories.categorySlug, listings.slug, listings.name, listings.id, categories.categoryName, listings.date');
    $this->db->from('listings');
    $this->db->join('items_categories', 'items_categories.listingId = listings.id');
    $this->db->join('categories', 'categories.id = items_categories.categoryId');
    $this->db->order_by("listings.date", "desc");
    $query = $this->db->get();
    return $query->result();
  }

  function NewListingsHome() {
    $this->db->limit(8);
    $this->db->select('categories.parentId, categories.categorySlug, listings.slug, listings.name, listings.id, categories.categoryName, listings.date');
    $this->db->from('listings');
    $this->db->join('items_categories', 'items_categories.listingId = listings.id');
    $this->db->join('categories', 'categories.id = items_categories.categoryId');
    $this->db->where('listings.approved', 1);
    $this->db->order_by("listings.date", "desc");
    $query = $this->db->get();
    return $query->result();
  }

  function RandomListingsHome() {
    $this->db->limit(8);
    $this->db->select('categories.parentId, categories.categorySlug, listings.slug, listings.name, listings.id, categories.categoryName');
    $this->db->from('listings');
    $this->db->join('items_categories', 'items_categories.listingId = listings.id');
    $this->db->join('categories', 'categories.id = items_categories.categoryId');
    $this->db->order_by("listings.name", "random");
    $query = $this->db->get();
    return $query->result();
  }
   
   function RandomListings() {
    $this->db->limit(5);
    $this->db->select('categories.parentId, categories.categorySlug, listings.slug, listings.name, listings.id, categories.categoryName');
    $this->db->from('listings');
    $this->db->join('items_categories', 'items_categories.listingId = listings.id');
    $this->db->join('categories', 'categories.id = items_categories.categoryId');
    $this->db->order_by("listings.name", "random");
    $query = $this->db->get();
    return $query->result();
  }

  function NewListingsCat($id_nosece) {
    $this->db->where('id', $id_nosece);
    $query = $this->db->get('categories');
    return $query->row_array();
  }

  function NewListingsImages($id) {
    $this->db->where('listingId', $id);
    $this->db->where('first', 1);
    $query = $this->db->get('images');
    return $query->row_array();
  }

  function NewListingVisits($id) {
    $this->db->where('listingId', $id);
    $query = $this->db->get('hits');
    return $query->row_array();
  }
  
  function CategoriesHome() {
    $this->db->where('parentId', 0);
    $this->db->order_by('categoryName', 'asc');
    $query = $this->db->get('categories');
    return $query->result();
  }
}