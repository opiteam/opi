<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class Tree {

    var $table = '';
    var $CI = '';
    var $ul_class = '';
    var $iteration_number = 0;
    function Tree($config = array()) {
        $this -> table = $config['table'];
        $this -> CI = &get_instance();
        $this -> CI -> load -> database();
        $this -> ul_class = $config['ul_class'];
    }

    function getTree($parent_id = 0) {
        $this -> iteration_number++;
        $this -> CI -> db -> where('id_nosece', $parent_id);
        $first_level = $this -> CI -> db -> get($this -> table) -> result();
        if($this->iteration_number == 1)
            $tree = '<select name="kategorija">';
        else
            $tree = '';
        foreach ($first_level as $fl) {
            $this -> CI -> db -> where('id_nosece', $fl -> id);
            $count = $this -> CI -> db -> count_all_results($this -> table);
            if ($count != 0) {
                $tree .= '<optgroup disabled><option>---' . $fl -> Ime_kategorije . '</option></optgroup>';
                $tree .= $this -> getTree($fl -> id);
            } else {
                $tree .= '<option value="'.$fl -> id.'">' . $fl -> Ime_kategorije . '</option>';
            }
        }
        return $tree;
    }
}
 ?>