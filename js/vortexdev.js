$(document).ready(function() {
     
  var base_url = $('#hiddenBaseUrl').val();
  var uploadfolder = $('#uploadfolder').val();  
  $('#file_upload').uploadify({
    'uploader'  : base_url + 'flash/uploadify.swf',
    'script'    : base_url + 'admin/uploadifyUploader',
    'cancelImg' : base_url + 'images/cancel.png',
    'folder'    : uploadfolder,
    'fileExt'     : '*.jpg;*.gif;*.png;*.zip;*.rar;*.flv;*.mp4;*.mp3',
    'auto'      : false,
    'multi'     : true,
     
     'onComplete'  : function(event, ID, fileObj, response, data) {
          
         $.ajax({
            url : base_url + 'admin/filemanipulation/' + fileObj.type +'/' + fileObj.name,
            success : function(response){
               
               if(response == 'image')
                 {
                    
                   var images = $('<input type="hidden" name="slika[]" value="'+fileObj.name+'" /><div id="firma_img"><a target="_blank" href="'+base_url+'uploads/'+fileObj.name+'"><img src="'+base_url + 'uploads/thumbs/' +fileObj.name+'" alt=""></a></div><a target="_blank" href="'+base_url+'uploads/'+fileObj.name+'">');
                   $(images).hide().insertBefore('#displayFiles').slideDown('slow')
                 }
                  else
                 {
                   var files = $('</a><a href="'+base_url + 'uploads/thumbs/' +fileObj.name+'" target="_blank">'+fileObj.name+'</a>');
                   $(files).hide().insertBefore('#displayFiles').slideDown('slow')
                 }
            }
        }) 
         
    }
  });
   
  
});