<?php
class NewsModel extends CI_Model {
    
  function item($id) {
    $this->db->where('id', $id);
    $query = $this->db->get('news');
    return $query->row_array();
  }

  function news($limit, $start) {
    $this->db->limit($limit, $start);
    $this->db->select('news.id, news.title, news.categoryId, news.slug, news.content, news.date, newscategories.slug, newscategories.name');
    $this->db->from('news');
    $this->db->join('newscategories', 'newscategories.id = news.categoryId');
    $this->db->order_by('news.date', 'desc');
    $query = $this->db->get();
    return $query->result();
  }

  function Category($link, $limit, $start) {
    $this->db->limit($limit, $start);
    $this->db->select('news.id, news.naslov, news.categoryId, news.slug, news.content, news.date, newscategories.slug, newscategories.name');
    $this->db->from('news');
    $this->db->join('newscategories', 'news.categoryId = newscategories.id');
    $this->db->where('newscategories.slug', $link);
    $this->db->order_by('news.date', 'desc');
    $query = $this->db->get();
    return $query->result();
  }

  function NewsPerCategoryCount($link) {
    $this->db->select();
    $this->db->from('news');
    $this->db->join('newscategories', 'news.categoryId = newscategories.id');
    $this->db->where('newscategories.slug', $link);
     $query = $this->db->get();
    return $query->num_rows(); 
  }

  function CategoryMenu() {
    $query = $this->db->get('newscategories');
    return $query->result();
  }
  
  function NewsHome() {
    $this->db->limit(4);
    $this->db->select('news.id, news.title, news.categoryId, news.slug, news.content, news.date, newscategories.slug, newscategories.name');
    $this->db->from('news');
    $this->db->join('newscategories', 'news.categoryId = newscategories.id');
    $this->db->order_by('date', 'desc');
    $query = $this->db->get();
    return $query->result();
  }
  
  function LastNewsHomeCat() {
    $query = $this->db->get('newscategories');
    return $query->result();
  }

  function LastNewsHome($id_kat) {
    $this->db->select('news.id, news.title, news.categoryId, news.slug, news.content, news.date, newscategories.slug, newscategories.name');
    $this->db->from('news');
    $this->db->join('newscategories', 'news.categoryid = newscategories.id');
    $this->db->where('categoryId', $id_kat);
    $this->db->order_by('date', 'desc');
    $this->db->limit(1);
    $query = $this->db->get();
    return $query->result();
  }
}