<?php $this->load->view('admin/header'); ?>
<div id="sadrzaj">
	<div id="sadrzaj_block">
		<div id="head_bg">
        	<div id="head_txt">Add Category</div>
    	</div> 
		<div id="lista">
			<div id="line"></div>
			<?php echo form_open_multipart('admin/listing/addCategory'); ?>
			<?php echo validation_errors('<div class="error">'); ?>
			<?php if( isset( $error) ) echo $error; ?>
			<table width="600">
			<tr>
			<td valign="top">
			<fieldset>
				<legend>Category Details</legend>
			<label>Name:</label>
			<?php echo form_input('name'); ?>
			<?php echo form_upload('userfile'); ?>
			</fieldset>
			</td>
			</tr>
			</table>
			<?php echo form_submit('submit', 'Add'); ?>
			<?php echo form_close(); ?>
			<div style="clear:both"></div>
		</div>
	</div>

<?php $this->load->view('admin/footer'); ?>