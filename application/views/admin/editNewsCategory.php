<?php $this->load->view('admin/header'); ?>
<div id="sadrzaj">
	<div id="sadrzaj_block">
		<div id="head_bg">
        	<div id="head_txt">Uredi kategoriju</div>
    	</div> 
		<div id="lista">
			<div id="line"></div>
			<?php echo form_open('admin/editNewsCategory'); ?>
			<?php echo validation_errors('<div class="error">'); ?>
			<table width="600">
			<tr>
			<td valign="top">
			<fieldset>
				<legend>Podaci o kategoriji</legend>
			<label>Ime Kategorije:</label>
			<?php foreach ($query as $q): ?>
			<?php echo form_input('ime_kategorije', $q->ime); ?>
			<input type="hidden" name="id" value="<?php echo $q->id ?>" />
			<?php endforeach ?>
			</fieldset>
			</td>
			</tr>
			</table>
			<?php echo form_submit('submit', 'Uredi'); ?>
			<?php echo form_close(); ?>
			<div style="clear:both"></div>
		</div>
	</div>

<?php $this->load->view('admin/footer'); ?>