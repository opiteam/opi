<?php $this->load->view('header'); ?>
<div class="main">
	<div class="container">
		<div class="row">
			<div class="span8">
				<div class="content">
					<div class="page-header">
						<h1>Online poslovni imenik</h1>
					</div>
					<div class="page">
						<div class="categories">
							<?php foreach ($query as $q): ?>
								<?php if ($q->id_nosece == 0): ?>
								<?php $id = $q->id; ?>
								<div class="well well-small">
									<a href="<?php echo base_url().'poslovni-imenik/'.$q->link_kategorije ?>">
										<div class="pull-left">
											<img src="<?php echo base_url().'uploads/kategorije/'.$q->slika ?>"/>
										</div>
										<h5><?php echo $q->Ime_kategorije ?><span class="bubble"><?php echo $this->site_model->broj_firmi_pokat($id) ?></span></h5>
									</a>
									<div style="clear: both"></div>
									<ul>
									<?php foreach ($this->site_model->subcategories($id) as $podkat): ?>
										<li><a href="<?php echo base_url().'poslovni-imenik/'.$q->link_kategorije.'/'.$podkat->link_kategorije ?>"><?php echo $podkat->Ime_kategorije ?>,</a></li>
									<?php endforeach ?>
									</ul>
									<div style="clear: both"></div>
								</div>
							<?php endif; ?>
							<?php endforeach ?>
						</div>
					</div>
				</div>
			</div>
			<div class="span4">
				<?php $this->load->view('site/moduli/tabovi_firme'); ?>
			</div>
		</div>
	</div>
<?php $this->load->view('footer'); ?>
 




	
