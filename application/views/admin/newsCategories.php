<?php $this->load->view('admin/header'); ?>
<div id="sadrzaj">
	<div id="sadrzaj_block">
	<div id="head_bg">
		<div id="head_txt">Lista kategorija</div>
		<div id="add_btn">
			<a href="<?php echo base_url()?>admin/vijesti/dodaj-kategoriju">Dodaj kategoriju</a>
		</div>
	</div>
	<div id="lista">
		<div id="naslov">Ime kategorije</div>
		<div id="line"></div>
		<?php foreach($query as $q) : ?>
			<div id="item">
				<a href="<?php echo base_url();?>admin/vijesti/kategorija/<?php echo $q->id ?>"><?php echo $q->ime; ?></a>
				<a id="removeNewsCategory" class="obrisi" href="#" title="obriši" data-id="<?php echo $q->id ?>"></a>
				<a class="uredi" title="uredi kategoriju" href="<?php echo base_url();?>admin/vijesti/uredi-kategoriju/<?php echo $q->id ?>"></a>
			</div>
		<?php endforeach ?>
		<div style="clear:both;"></div>
	</div>
	<?php echo $link ?>
</div>
<?php $this->load->view('admin/footer'); ?>