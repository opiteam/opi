<?php $this->load->view('site/header'); ?>

<div class="main">
	<div class="container">
		<div class="row">
			<div class="span8">
				<div class="content">
					<div class="page-header">
						<h1><?php echo $kat_ime['Ime_kategorije'] ?></h1>
					</div>
					<div class="page">
						<div class="podkat">
									<?php if ($query): ?>
									<?php foreach ($query as $q): ?>
										<?php $id = $q->id; ?>
										<?php $slike = $this->site_model->slike_lista($id); ?>
										<div class="panel1 panel1-primary" style="min-height: 150px;">
											<div class="panel1-heading">
												<a href="<?php echo base_url().'poslovni-imenik/item/'.$q->id.'/'.$q->link?>"><?php echo $q->Ime ?></a>
											</div>
											<div class="pull-left">
												<?php if ($slike): ?>
												<?php foreach ($slike as $slika): ?>
												<?php if ($slika->prva == 1): ?>
												<img class="thumbnail slika" src="<?php echo base_url().'uploads/thumbs/'.$slika->ime ?>" />
												<?php endif ?>
												<?php endforeach ?>
												<?php else: ?>
												<img class="thumbnail slika" src="<?php echo base_url().'images/nema-slike.jpg' ?>" />
												<?php endif ?>
											</div>
											<div class="text">
												<?php echo character_limiter($q->Opis, 250) ?>
											</div>
											<a class="vise" href="<?php echo base_url().'poslovni-imenik/item/'.$q->id.'/'.$q->link?>">Više o firmi</a>
											<div style="clear: both"></div>
										</div>	

									<?php endforeach ?>
									<?php else: ?>
					Trenutno nema firmi u kategoriji
				    <?php endif ?>
									<div style="clear: both"></div>
						</div>
						<?php echo $link; ?>
					</div>
				</div>
			</div>
			<div class="span4">
				<?php $this->load->view('site/moduli/tabovi_firme'); ?>
			</div>
		</div>
	</div>
<?php $this->load->view('site/footer'); ?>
 




	
