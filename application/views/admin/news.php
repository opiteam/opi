<?php $this->load->view('admin/header'); ?>
<div id="sadrzaj">
	<div id="sadrzaj_block">
	<div id="head_bg">
		<div id="head_txt">Vijesti</div>
		<div id="add_btn">
			<a href="<?php echo base_url()?>admin/vijesti/dodaj-vijest">Dodaj Vijest</a>
		</div>
		<div id="add_btn">
			<a href="<?php echo base_url()?>admin/vijesti/dodaj-kategoriju">Dodaj Kategoriju</a>
		</div>
		<div id="add_btn">
			<a href="<?php echo base_url()?>admin/NewsCategories">Kategorije</a>
		</div>
	</div>
	<div id="lista">
		<div id="naslov">Naslov</div>
		<div id="line"></div>
		
		<select name="goToUrl" onchange="location.href='<?php echo base_url()?>admin/vijesti/kategorija/' + this.options[this.selectedIndex].value;">
			<option value="">--Odaberite kategoriju--</option>
			<?php foreach ($kateg as $k):?>
			
			<option value="<?php echo $k->id ?>"><?php echo $k->ime ?></option>
		
	<?php endforeach ?>
	</select>
		<?php foreach($query as $q) : ?>
			<div id="item">
				<a href="<?php echo base_url();?>admin/vijesti/uredi-vijest/<?php echo $q->id ?>"><?php echo $q->naslov; ?></a>
				<a id="removeNewsItem" class="obrisi" href="#" title="obriši" data-id="<?php echo $q->id ?>"></a>
				<div class="autor">Kategorija: <strong><?php echo $q->ime ?></strong></div>
			</div>
		<?php endforeach ?>
		<div style="clear:both;"></div>
	</div>
	<?php echo $link ?>
</div>
<?php $this->load->view('admin/footer'); ?>