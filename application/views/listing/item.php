<?php $this->load->view('site/header'); ?>
<script>
$(document).ready(function() {
	initialize();
});
</script>
<?php if ($query): ?>
<?php $page = $query['id'];
	$result = mysql_query("SELECT brojac FROM  firme_posjete WHERE id_firme = '$page'");

	$any = false;
	while($row = mysql_fetch_array($result)) {
		$any = true;
		$counter = mysql_result($result, 0) + 1;
		$sql = "UPDATE firme_posjete SET brojac = '$counter' WHERE id_firme = '$page'";
		mysql_query($sql);
	}

	if($any == false) {
		$sql = "INSERT INTO firme_posjete (id_firme, brojac) VALUES ('$page', '1')";
		mysql_query($sql);
	}
?>
<div class="main">
	<div class="container">
		<div class="row">
			<div class="span8">
				<div class="content">
					<div class="page-header">
						<h1><?php echo $query['Ime'] ?></h1>
					</div>
					<div class="page">
						<div class="item">
							<div class="span4">
								<p><?php echo $query['Opis'] ?></p>
							</div>
							<div class="pull-right">
								<div class="span3">
									<?php foreach ($slike as $slika): ?>
									<?php if ($slika->prva == 1): ?>
									<center><img class="thumbnail" src="<?php echo base_url().'uploads/thumbs/'.$slika->ime ?>" /></center>
									<div style="clear: both"></div>
									<?php else: ?>
									<a rel="prettyPhoto" href="<?php echo base_url().'uploads/'.$slika->ime ?>">
									<img class="mala" src="<?php echo base_url().'uploads/thumbs/'.$slika->ime ?>" />
								    </a>
									<?php endif ?>
									<?php endforeach ?>
									<?php if (!$slike): ?>
									<center><img class="thumbnail" src="<?php echo base_url().'images/nema-slike.jpg' ?>" /></center>
									<?php endif ?>
									<h2>
										<i class="icon-briefcase"></i>
										Detalji <span>firme</span>
									</h2>
									<table class="table table-striped .table-condensed">
										<?php if ($query['Adresa']): ?>
										<tr>
											<td>Adresa:</td>
											<td><?php echo $query['Adresa'] ?></td>
										</tr>
										<?php endif ?>
										<?php if ($query['Postanski_br']): ?>
										<tr>
											<td>Poštanski broj:</td>
											<td><?php echo $query['Postanski_br'] ?></td>
										</tr>
										<?php endif ?>
										<?php if ($query['Kanton']): ?>
										<tr>
											<td>Kanton:</td>
											<td><?php echo $query['Kanton'] ?></td>
										</tr>
										<?php endif ?>
										<?php if ($query['Grad']): ?>
										<tr>
											<td>Grad:</td>
											<td><?php echo $query['Grad'] ?></td>
										</tr>
										<?php endif ?>
										<?php if ($query['Drzava']): ?>
										<tr>
											<td>Država:</td>
											<td><?php echo $query['Drzava'] ?></td>
										</tr>
										<?php endif ?>
										<?php if ($query['Telefon1']): ?>
										<tr>
											<td>Telefon:</td>
											<td><?php echo $query['Telefon1'] ?></td>
										</tr>
										<?php endif ?>
										<?php if ($query['Telefon2']): ?>
										<tr>
											<td>Telefon 2:</td>
											<td><?php echo $query['Telefon2'] ?></td>
										</tr>
										<?php endif ?>
										<?php if ($query['Telefon3']): ?>
										<tr>
											<td>Telefon 3:</td>
											<td><?php echo $query['Telefon3'] ?></td>
										</tr>
										<?php endif ?>
										<?php if ($query['Telefon4']): ?>
										<tr>
											<td>Telefon 4:</td>
											<td><?php echo $query['Telefon4'] ?></td>
										</tr>
										<?php endif ?>
										<?php if ($query['Fax']): ?>
										<tr>
											<td>Fax:</td>
											<td><?php echo $query['Fax'] ?></td>
										</tr>
										<?php endif ?>
										<?php if ($query['Email1']): ?>
										<tr>
											<td>Email:</td>
											<td><a href="mailto:<?php echo $query['Email1'] ?>"><?php echo $query['Email1']?></a></td>
										</tr>
										<?php endif ?>
										<?php if ($query['Email2']): ?>
										<tr>
											<td>Email 2:</td>
											<td><a href="mailto:<?php echo $query['Email2'] ?>"><?php echo $query['Email2']?></a></td>
										</tr>
										<?php endif ?>
										<?php if ($query['webstranica']): ?>
										<tr>
											<td>Web stranica:</td>
											<td><a href="<?php echo prep_url($query['webstranica']) ?>"><?php echo $query['webstranica']?></a></td>
										</tr>
										<?php endif ?>
									</table>
								</div>
							</div>
							<div class="span8">
								<?php if ($query['lat'] && $query['lng'] > 0):?>
								<h2>Mapa</h2>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script>
var geocoder = new google.maps.Geocoder();

function geocodePosition(pos) {
  geocoder.geocode({
    latLng: pos
  });
}


function updateMarkerPosition(latLng) {
  $('#lat').val(latLng.lat());
  $('#lng').val(latLng.lng());
 
}


function initialize() {
  var latLng = new google.maps.LatLng(<?php echo $query['lat']?>,<?php echo $query['lng']?>);
  var map = new google.maps.Map(document.getElementById('map-canvas'), {
    zoom: <?php echo $query['zoom']?>,
    center: latLng,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  });
  var marker = new google.maps.Marker({
    position: latLng,
    title: 'Point A',
    map: map,
    draggable: false
  });
}

// Onload handler to fire off the app.
google.maps.event.addDomListener(window, 'load', initialize);

 </script>

<div id="map-canvas"></div>
								<?php endif ?>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="pull-right">
				<div class="span4">
					<?php $this->load->view('site/moduli/tabovi_firme'); ?>
				</div>
			</div>
		</div>
	</div>
<?php else: ?>
	<h2>Nema firme u Bazi</h2>
<?php endif ?>
<?php $this->load->view('site/footer'); ?>
 




	
