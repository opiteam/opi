﻿<?php
$NewsCategories = $this->NewsModel->CategoryMenu();
$newsNumber = $this->AdminModel->NewsCount();
$listingNumber = $this->AdminModel->itemsCount();
$configuration = $this->AdminModel->get_configuration();
$website_name = $configuration['SiteName'];
$website_desc = $configuration['SiteDesc'];
$language = 'english';
$this->lang->load('site', $language);
?>
<html>
<head>
	<title><?php if ($title) {echo $title;}?> <?php echo ' - '. $website_name; ?></title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="<?php echo strip_tags(character_limiter($website_desc, 100)) ?>">
	<meta name="author" content="DAMedia">
	<meta name="title" content="<?php if ($title) {echo $title;} else {echo $website_name;}?>">
	<meta property="og:site_name" content="<?php echo $website_name ?>">
	<meta property="og:image" content="<?php echo $image; ?>">
	<link rel="shortcut icon" href="http://images.opi.ba/favicon.ico">
	<script src="<?php echo base_url() ?>/js/jquery.js"></script>
    <script src="<?php echo base_url() ?>/js/site.js"></script>
    
    <script src="http://js.opi.ba/bootstrap.js"></script>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>/css/bootstrap.min.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>/css/bootstrap-responsive.min.css">
	<link href="<?php echo base_url() ?>/css/style.css" type="text/css" rel="stylesheet" />
	<link href="http://fmm.ba/~almedin/css/skin.css" rel="stylesheet" />
	<link rel="stylesheet" href="<?php echo base_url() ?>/css/animate.css">
	<link rel="stylesheet" href="<?php echo base_url() ?>/css/liquid-slider.css">
	
</head>
<body>
	<div class="layout">
			<header>
				<div class="header">
	   				<div class="container">
	      				<div class="row">
	      					<div class="span4">
	      						<div class="logo">
	      							<img src="<?php echo base_url().'images/'.$configuration['Logo']; ?>" />
	      						</div>
	      					</div>
	      					<div class="span8">
	      						<div class="ad_velika mobitel_sakrij">
	      							<a href="http://demofest.org"><img src="<?php echo base_url()?>images/marketing/demofest_novi.jpg" /></a>
	      						</div>
	      					</div>
	      				</div>
					</div>
					</header>
					<nav id="nav" class="navigation">
						<div class="container">
							<div class="row">
								<div class="span12">
									<ul class="menu fading">
										<li>
											<a <?php if ($this->uri->segment(1)=='') echo 'class="active"';?> href="<?php echo base_url() ?>"><?php echo $this->lang->line('home');?></a>
										</li>
										<li>
											<a <?php if ($this->uri->segment(2)=='news' || $this->uri->segment(2)=='news') echo 'class="active"';?> href="<?php echo base_url('news') ?>"><?php echo $this->lang->line('BussinesNews');?></a>
											<ul>
												<li><a href="<?php echo base_url()?>poslovne-novosti/kako-uloziti-vas-novac">Kako uloÅ¾iti VaÅ¡ novac</a></li>
											</ul>
										</li>
										<li><a <?php if ($this->uri->segment(1)=='poslovni-imenik') echo 'class="active"';?> href="<?php echo base_url() ?>poslovni-imenik"><?php echo $this->lang->line('BussinesDirectory');?> <span class="balon"><?php echo $listingNumber ?></span></a></li>
										<li><a <?php if ($this->uri->segment(1)=='dodaj-firmu') echo 'class="active"';?> href="<?php echo base_url() ?>dodaj-firmu"><?php echo $this->lang->line('AddListingFree');?></a></li>
										<li><a <?php if ($this->uri->segment(1)=='kontakt') echo 'class="active"';?> href="<?php echo base_url() ?>kontakt"><?php echo $this->lang->line('Contact');?></a></li>
									</ul>
								</div>
							</div>
						</div>
					</nav>
					<div class="search">
						<div class="container">
							<div class="row">
								<div class="span6">
									<div class="add">
										<a href="<?php echo base_url()?>dodaj-firmu" class="btn btn btn-small">
											<i class="icon-plus"></i>
											<span><?php echo $this->lang->line('GetYourListingOnDirectory');?></span>
										</a>
									</div>
								</div>
							 
							
								<div class="span6">
									<div class="pull-right">
										<form action="<?php echo base_url()?>site/pretraga_widget" method="post" class="form-inline">
											<input autocomplete="off" name="pretraga" data-provide="typeahead" type="text" size="20" class="search inputbox input-xlarge" value placeholder="<?php echo $this->lang->line('SearchForListing');?>" />
											<button class="btn" data-togle="tooltip" title data-original-title="<?php echo $this->lang->line('Search');?>">
												<i class="icon-search"></i>
												<span><?php echo $this->lang->line('Search');?></span>
											</button>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
	   			</div>
	   		<?php if ($this->session->flashdata('notification')): ?>
      		<div class="alert alert-success"><?php echo $this->session->flashdata('notification'); ?></div>
    		<?php endif ?>

