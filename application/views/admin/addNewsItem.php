<?php $this->load->view('admin/header'); ?>
<div id="sadrzaj">
	<div id="sadrzaj_block">
	<div id="head_bg">
		<div id="head_txt">Dodaj Novu vijest</div>
	</div>
	<div id="lista">
		<div id="line"></div>

<?php echo form_open('admin/AddNewsItem'); ?>
<?php echo validation_errors('<div class="error">'); ?>
<table width="600">
		<tr>
			<td valign="top">
	<fieldset>
		<legend>Detalji o vijesti</legend>
<label>Naslov:</label>
<?php echo form_input('naslov'); ?>
<?php
    
    if ($this->uri->segment(4)) {
        $id_kat = $this->uri->segment(4);
    echo '<input type="hidden" name="kategorija" value="'.$id_kat.'"/>';

}

else {
    echo '<label>Kategorija:</label>';
    echo '<select name="kategorija">';
    foreach ($kateg as $kat){
    
    $kat_id = $kat->id;
    $kat_ime = $kat->ime;
    echo <<<html
    <option value="{$kat_id}">{$kat_ime}</option>

html;
}
    echo '</select>';
} ?>
<label>Sadržaj:</label>
<textarea class="tiny" name="sadrzaj"></textarea>

</fieldset>
</td>
</td>
</tr>
</table>
<?php echo form_submit('submit', 'Dodaj vijest'); ?>
<?php echo form_close(); ?>
<div style="clear:both;"></div>
</div>
</div>

<?php $this->load->view('admin/footer'); ?>