<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
 <head>
   <title><?php if ($title) {echo $title;} else {echo "Opi.ba";}?> - Administracija</title>
   <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
   <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/admin.css" />
   <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/bootstrap.min.css" />
   <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/jquery.multifile.min.css" />
   <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/image_crud/css/file_upload/file-uploader.css" />
   <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/image_crud/css/file_upload/fileuploader.css" />
   <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/image_crud/css/file_upload/jquery.fileupload-ui.css" />
   <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/image_crud/css/photogallery.css" />
   <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/image_crud/css/fineuploader.css" />
   <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/image_crud/css/fileuploader.css" />
   <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/image_crud/css/colorbox.css" />
   <script src="<?php echo base_url();?>js/jquery.js"></script>
   <script src="<?php echo base_url();?>js/site.js"></script>
   <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
   <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
   <script type="text/javascript" src="<?php echo base_url();?>js/tinymce/tinymce.min.js"></script>  
   <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
   <script src="<?php echo base_url();?>assets/image_crud/js/jquery.colorbox-min.js"></script>
   <script src="<?php echo base_url();?>assets/image_crud/js/fileuploader.js"></script>
   <script src="<?php echo base_url();?>assets/image_crud/js/fineuploader-3.2.min.js"></script>
   <script src="<?php echo base_url();?>assets/image_crud/js/jquery.fileupload.js"></script>
   <script src="<?php echo base_url();?>assets/image_crud/js/jquery.colorbox-min.js"></script>

 </head>

<script type="text/javascript">
tinymce.init({
    selector: "textarea.tiny",
    theme: "modern",
    width: 640,
    plugins: [
        "advlist autolink lists link image charmap print preview anchor",
        "searchreplace visualblocks code fullscreen",
        "insertdatetime media table contextmenu paste"
    ],
    image_advtab: true,
    invalid_elements : "div,em",
    convert_urls: false,

    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",

});
</script>

 <script>
  $(function() {
    $( "#datepicker" ).datepicker({ dateFormat: "dd.mm.yy" });
  });
  </script>
 <script>
 $(document).ready(function() {
 $('#item #obrisi_kat').click(function(e) {

 	e.preventDefault();
 	if (confirm('Jeste li sigurni da želite obrisati kategoriju?'))
 	{
 		var link = $(this);
 		$.ajax({
 			url: '<?php echo base_url()?>admin/removeCategory/' + link.data('id'),
 			dataType: 'json',
 			success: function(data) {
               link.parents('#item').fadeOut('fast', function() {
                  $(this).remove();
               });
            }
 		})
 	}
})

 $('#item #removeNewsItem').click(function(e) {

  e.preventDefault();
  if (confirm('Jeste li sigurni da želite obrisati vijest?'))
  {
    var link = $(this);
    $.ajax({
      url: '<?php echo base_url()?>admin/removeNewsItem/' + link.data('id'),
      dataType: 'json',
      success: function(data) {
               link.parents('#item').fadeOut('fast', function() {
                  $(this).remove();
               });
            }
    })
  }
})

 $('#item #removeNewsCategory').click(function(e) {

  e.preventDefault();
  if (confirm('Jeste li sigurni da želite obrisati kategoriju?'))
  {
    var link = $(this);
    $.ajax({
      url: '<?php echo base_url()?>admin/removeNewsCategory/' + link.data('id'),
      dataType: 'json',
      success: function(data) {
               link.parents('#item').fadeOut('fast', function() {
                  $(this).remove();
               });
            }
    })
  }
})

 $('#item #removeUser').click(function(e) {

  e.preventDefault();
  if (confirm('Jeste li sigurni da želite obrisati korisnika?'))
  {
    var link = $(this);
    $.ajax({
      url: '<?php echo base_url()?>admin/removeUser/' + link.data('id'),
      dataType: 'json',
      success: function(data) {
               link.parents('#item').fadeOut('fast', function() {
                  $(this).remove();
               });
            }
    })
  }
})

  /*$('#item #obrisi_aip').click(function(e) {

   e.preventDefault();
   if (confirm('Jeste li sigurni da želite obrisati akciju ili promociju?'))
   {
      var link = $(this);
      $.ajax({
         url: '<?php echo base_url()?>admin/obrisi_aip/' + link.data('id'),
         dataType: 'json',
         success: function(data) {
               link.parents('#item').fadeOut('fast', function() {
                  $(this).remove();
               });
            }
      })
   }
})*/

 $('#item_img #removeImage').click(function(e) {

   e.preventDefault();
   if (confirm('Jeste li sigurni da želite obrisati sliku?'))
   {
      var link = $(this);
      $.ajax({
         url: '<?php echo base_url()?>admin/removeImage/' + link.data('id'),
         dataType: 'json',
         success: function(data) {
               link.parents('#item_img').fadeOut('fast', function() {
                  $(this).remove();
               });
            }
      })
   }
})

  $('#item #obrisi_podkat').click(function(e) {

   e.preventDefault();
   if (confirm('Jeste li sigurni da želite obrisati podkategoriju?'))
   {
      var link = $(this);
      $.ajax({
         url: '<?php echo base_url()?>admin/removeSubcategory/' + link.data('id'),
         dataType: 'json',
         success: function(data) {
               link.parents('#item').fadeOut('fast', function() {
                  $(this).remove();
               });
            }
      })
   }
})

   $('#item #removeItem').click(function(e) {

   e.preventDefault();
   if (confirm('Jeste li sigurni da želite obrisati firmu?'))
   {
      var link = $(this);
      $.ajax({
         url: '<?php echo base_url()?>admin/removeItem/' + link.data('id'),
         dataType: 'json',
         success: function(data) {
               link.parents('#item').fadeOut('fast', function() {
                  $(this).remove();
               });
            }
      })
   }
})



});

function brisi_aip() {

if (confirm('Jeste li sigurni da želite obrisati akciju ili promociju?'))
   {
      var link = $(this);
      $.ajax({
         url: '<?php echo base_url()?>admin/obrisi_aip/' + link.data('id'),
         dataType: 'json',
         success: function(data) {
               link.parents('#item').fadeOut('fast', function() {
                  $(this).remove();
               });
            }
      })
   }
 }
</script>
 <body>
<div id="content">
    <?php if ($this->session->flashdata('notification')): ?>
      <div class="alert alert-success"><?php echo $this->session->flashdata('notification'); ?></div>
    <?php endif ?>
<?php $this->load->view('admin/sidebar'); ?>

  