<?php $this->load->view('header'); ?>
<div class="main">
	<div class="container">
		<div class="row">
			<div class="span8">
				<div class="content">
					<div class="page-header">
						<h1>Poslovne novosti</h1>
					</div>
					<div class="page">

						<?php foreach ($query as $q): ?>
						<?php
						$pic = $q->content;
 						preg_match_all('/<img[^>]+>/i', $pic, $output, PREG_PATTERN_ORDER); ?>
						<div class="vijesti_block">
						<div class="row">
							<div class="span3">
									<?php foreach ($output as $out) : ?>
										<div class="slicica">
											<?php echo $out[0] ?>
										</div>
							</div>
							<div class="span5">
									<h3><a href="<?php echo base_url().'poslovne-novosti/vijest/'.$q->id.'/'.$q->slug; ?>"><?php echo $q->title ?></h3></a>
									<div class="objavljeno"><i class="icon-time"></i> Objavljeno <?php echo date('d.m.Y', strtotime($q->date)).' u kategoriji <em><a href="poslovne-novosti/'.$q->slug.'">'.$q->name.'</a></em>' ?> </div> 
									<p><?php echo preg_replace('/<img[^>]+>/i', ' ', strip_tags(character_limiter($q->content, 400))); ?></p>
									<div style="clear: both"></div>
									<a  href="<?php echo base_url().'poslovne-novosti/vijest/'.$q->id.'/'.$q->slug; ?>"><i class="icon-circle-arrow-right"></i> Pročitajte više</a>
									<hr>
							</div>
							</div>
							</div>
						<?php endforeach ?>
						<?php endforeach ?>
						<?php echo $link; ?>
					</div>
				</div>
			</div>
			<div class="span4">
				<?php $this->load->view('modules/categoryNewsMenu'); ?>
				
			</div>
		</div>
	</div>
<?php $this->load->view('footer'); ?>
 




	
