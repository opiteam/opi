<?php $this->load->view('admin/header'); ?>
<script>
$(document).ready(function() {
     initialize();
})
</script>
<div id="sadrzaj">
	<div id="sadrzaj_block">
	<div id="head_bg">
		<div id="head_txt">Dodaj Novu Firmu</div>
	</div>
	<div id="lista">
		<div id="line"></div>
<table width="1000">
        <tr>
            <td valign="top">
<?php echo form_open_multipart('admin/addItem'); ?>
<?php echo validation_errors('<div class="error">'); ?>
	<fieldset>
		<legend>Detalji o firmi</legend>
<label>Ime Firme:</label>
<?php echo form_input('ime_firme'); ?>
<?php
    
    if ($this->uri->segment(4)) {
        $id_kat = $this->uri->segment(4);
    echo '<input type="hidden" name="kategorija" value="'.$id_kat.'"/>';

}

else {
    echo '<label>Kategorija:</label>';
    echo $tree;
    echo '</select>';
} ?>
<label>Opis Firme:</label>
<textarea name="opis_firme" class="tiny"></textarea>
<label>Adresa Firme:</label>
<?php echo form_input('adresa'); ?>
<label>Poštanski broj:</label>
<?php echo form_input('postanski'); ?>
<label>Grad:</label>
<?php echo form_input('grad'); ?>
<label>Kanton:</label>
<?php echo form_input('kanton'); ?>
<label>Država:</label>
<?php echo form_input('drzava'); ?>
<label>Telefon 1:</label>
<?php echo form_input('telefon1'); ?>
<label>Telefon 2:</label>
<?php echo form_input('telefon2'); ?>
<label>Telefon 3:</label>
<?php echo form_input('telefon3'); ?>
<label>Telefon 4:</label>
<?php echo form_input('telefon4'); ?>
<label>Fax:</label>
<?php echo form_input('fax'); ?>
<label>Email 1:</label>
<?php echo form_input('email1'); ?>
<label>Email 2:</label>
<?php echo form_input('email2'); ?>
<label>Web stranica: (www.primjer.ba)</label>
<?php echo form_input('web', set_value('web')); ?>
</fieldset>
</td>
<td valign="top" width="350">
<fieldset>
<legend>Slike</legend>
<div class="well" style="padding: 14px 19px;">
    <input type="file" name="userfile[]">
    <input type="file" name="userfile[]">
    <input type="file" name="userfile[]">
    <input type="file" name="userfile[]">
    <input type="file" name="userfile[]">
    <input type="file" name="userfile[]">
    <input type="file" name="userfile[]">
    <input type="file" name="userfile[]">
    <input type="file" name="userfile[]">
    <input type="file" name="userfile[]">
</div>
</fieldset>
<fieldset>
<legend>Mapa</legend>
<script type="text/javascript"
      src="http://maps.google.com/maps/api/js?sensor=false">
    </script>
<script type="text/javascript">
  function initialize() {
    var myLatlng = new google.maps.LatLng(44.21371012604076,17.753906375000042);
    var mapTypeIds = [];
            for(var type in google.maps.MapTypeId) {
                mapTypeIds.push(google.maps.MapTypeId[type]);
            }
    mapTypeIds.push("OSM");
    var myOptions = {
      zoom: 7,
      center: myLatlng,
      mapTypeId: "OSM",
      mapTypeControlOptions: {
                    mapTypeIds: mapTypeIds
                }
    }
    var map = new google.maps.Map(document.getElementById("map-canvas"), myOptions);
    
    var marker = new google.maps.Marker({
        position: myLatlng, 
        map: map,
        draggable:true
    });
    google.maps.event.addListener(
        marker,
        'dragend',
        function() {
            document.getElementById('lat').value = marker.position.lat();
            document.getElementById('lng').value = marker.position.lng();
            document.getElementById('zoom').value = map.getZoom();
        }
    );
    marker.setMap(map);           
 
            map.mapTypes.set("OSM", new google.maps.ImageMapType({
                getTileUrl: function(coord, zoom) {
                    return "http://tile.openstreetmap.org/" + zoom + "/" + coord.x + "/" + coord.y + ".png";
                },
                tileSize: new google.maps.Size(256, 256),
                maxZoom: 18
            }));
  }
</script>
<input type="hidden" id="lat" name="lat" />
<input type="hidden" id="lng" name="lng" />
<input type="hidden" id="zoom" name="zoom" />
<div id="map-canvas"></div>
</fieldset>
</td>
</tr>
</table>
<?php echo form_submit('submit', 'Dodaj'); ?>
<?php echo form_close(); ?>
<div style="clear:both;"></div>
</div>
</div>

<script type="text/javascript" src="<?=base_url()?>js/swfobject.js"></script>
<script type="text/javascript" src="<?=base_url()?>js/jquery.uploadify.v2.1.4.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>js/vortexdev.js"></script>
<?php $this->load->view('admin/footer'); ?>