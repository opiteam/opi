<?php $this->load->view('admin/header'); ?>
<div id="sadrzaj">
	<div id="sadrzaj_block">
	<div id="head_bg">
		<div id="head_txt">Edit custom page</div>
	</div>
	<div id="lista">
		<div id="line"></div>

<?php echo form_open('admin/add_custom_page'); ?>
<?php echo validation_errors('<div class="error">'); ?>
<table width="600">
		<tr>
			<td valign="top">
	<fieldset>
		<legend>Custom page details</legend>
<label>Naslov:</label>
<?php echo form_input('title'); ?>

<label>Content:</label>
<textarea class="tiny" name="content"></textarea>

</fieldset>
</td>
</td>
</tr>
</table>
<?php echo form_submit('submit', 'Edit Custom page'); ?>
<?php echo form_close(); ?>
<div style="clear:both;"></div>
</div>
</div>

<?php $this->load->view('admin/footer'); ?>