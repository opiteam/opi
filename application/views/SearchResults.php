<?php $this->load->view('site/header'); ?>

<div class="main">
	<div class="container">
		<div class="row">
			<div class="span8">
				<div class="content">
					<div class="page-header">
						<h1>Rezultati pretrage</h1>
					</div>
					<div class="page">
						<div class="podkat">
									<?php if($query):?>
									<h2>Firme</h2>
									<hr class="dotted">
									<ul>
									<?php foreach ($query as $q): ?>
										<?php $id = $q->id; ?>
										
											<li><a href="<?php echo base_url().'poslovni-imenik/item/'.$q->id.'/'.$q->link?>"><?php echo $q->Ime ?></a></li>
										
											
											<div style="clear: both"></div>	

									<?php endforeach ?>
									</ul>
								<?php else: ?>
									<?php echo "Nema nijedne pronađene firme"; ?>
								<?php endif ?>
									<div style="clear: both"></div>
									<?php if($query_v):?>
									<h2>Vijesti</h2>
									<hr class="dotted">
									<ul>
									<?php foreach ($query_v as $q): ?>
										<?php $id = $q->id; ?>
										
											<li><a href="<?php echo base_url().'poslovne-novosti/vijest/'.$q->id.'/'.$q->link; ?>"><?php echo $q->naslov ?></a></li>
									
											
											<div style="clear: both"></div>	

									<?php endforeach ?>
									</ul>
									<?php else: ?>
									<?php echo "Nema nijedne pronađene vijesti"; ?>
								<?php endif ?>
									<div style="clear: both"></div>

						</div>
					</div>
				</div>
			</div>
			<div class="span4">
				<?php $this->load->view('site/moduli/tabovi_firme'); ?>
			</div>
		</div>
	</div>
<?php $this->load->view('site/footer'); ?>
 




	
