﻿<?php $this->load->view('header'); ?>
<div class="main">
	<div class="container">
		<div class="row">
			<div class="span8">
				<div class="content">
					<div class="page-header">
								<h2>
									<span>Poslovne</span> novosti
								</h2>
							</div>
					<div class="nove_vijesti">
						<div class="prikazi-sve">
								<a href="<?php echo base_url('news')?>">
									Prikazati sve
								</a>
							</div>
						<div class="row animated bounceInLeft" style="opacity: 1;">
						<?php
							$i = 0;
							foreach ($query6 as $q): ?>
    						<?php if ($i == 0) : ?>
    							<?php
								$pic = $q->content;
 								preg_match_all("<img.*?src=[\"\"'](?<url>.*?)[\"\"'].*?>", $pic, $output, PREG_PATTERN_ORDER); ?>
        					  	<div class="span4">
        					  		<div class="opis">
        					  			<h4 style="margin-bottom: 25px">
        					  				<a href="<?php echo base_url().'poslovne-novosti/vijest/'.$q->id.'/'.$q->slug; ?>">
        					  					<?php echo $q->title ?>
        					  				</a>
        					  			</h4>
        					  			<?php
        					  			$xpath = new DOMXPath(@DOMDocument::loadHTML($pic));
										$src = $xpath->evaluate("string(//img/@src)"); 
										echo '<img style="margin-bottom: 25px;" src="'.$src.'">';
										?>
        					  			<!--<?php foreach ($output as $out) : ?>
									
										<?php echo '<img src="'.$out[0].'">'; ?>
										<?php endforeach ?>-->
										<div style="clear: both"></div>
        					  			<?php echo preg_replace('/src=[\'"]?([^\'" >]+)[\'" >]/', ' ', strip_tags(character_limiter($q->content, 400))); ?>
        					  			<ul class="unstyled">
        					  			<?php if($configuration['ShowPublished'] == 1):?>
 										<li>
 											<small>
 												<i class="icon-calendar"></i>
 												Published <?php echo date('d.m.Y', strtotime($q->date)) ?>
 											</small>
 										</li>
 									<?php endif ?>
 										<li>
 											<small>Category: <a href="poslovne-novosti/<?php echo $q->slug ?>"><?php echo character_limiter($q->name, 21) ?></a></small>
 										</li>
 									<ul>
        					  		</div>
        					  	</div>
 							<?php else: ?>
 							<?php 
 							$pic = $q->content;
 							preg_match_all('/<img[^>]+>/i', $pic, $output, PREG_PATTERN_ORDER); ?>
 							<div class="span4">
 								<div class="vijesti_na_pocetnoj">
 							<?php foreach ($output as $out) : ?>
 								<div class="vijest">
 									<div class="slicica">
 										<a href="<?php echo base_url().'poslovne-novosti/vijest/'.$q->id.'/'.$q->slug; ?>">
 											<?php echo $out[0] ?>
 										</a>
 									</div>
 									<article>
 									<h4>
 										<a href="<?php echo base_url().'poslovne-novosti/vijest/'.$q->id.'/'.$q->slug; ?>">
 											<?php echo $q->title ?>
 										</a>
 									</h4>
 									<div style="clear: both"></div>

 									<ul class="unstyled">
 										<?php if($configuration['ShowPublished'] == 1):?>
 										<li>
 											<small>
 												<i class="icon-calendar"></i>
 												Published <?php echo date('d.m.Y', strtotime($q->date)) ?>
 											</small>
 										</li>
 										<?php endif; ?>
 										<li>
 											<small>Category: <a href="poslovne-novosti/<?php echo $q->slug ?>"><?php echo character_limiter($q->name, 21) ?></a></small>
 										</li>
 									<ul>
 									</article>
 								</div>
 							<?php endforeach ?>
 						        </div>
 						    </div>
    						<?php endif ?> 

    					<?php $i++; ?>
						<?php endforeach ?>
						</div>
					</div>
					<hr class="dotted">
					<div class="page-header">
						<h2>
							<span>Zadnje vijesti</span> po kategorijama
						</h2>
					</div>
					
					<?php foreach($query8 as $q8): ?>
						
						<?php $vijesti = $this->NewsModel->LastNewsHome($q8->id) ?>
						<?php foreach($vijesti as $vij): ?>

						<?php 
 						$pic = $vij->content;
 						preg_match_all('/<img[^>]+>/i', $pic, $output, PREG_PATTERN_ORDER); ?>
 				
 						<div class="zad_vijesti_poc">
 						<?php foreach ($output as $out) : ?>
							<div class="slika">
								<a href="<?php echo base_url().'poslovne-novosti/vijest/'.$vij->id.'/'.$vij->slug; ?>">
									<?php echo $out[0] ?>
								</a>
							</div>
						<?php endforeach ?>
							<h5>
								<a href="<?php echo base_url().'poslovne-novosti/vijest/'.$vij->id.'/'.$vij->slug; ?>">
									<?php echo character_limiter($vij->title, 30) ?>
								</a>
							</h5>
							<small>
								Zadnje u kategoriji: <a href="<?php echo base_url().'poslovne-novosti/'.$vij->slug; ?>"><?php echo $vij->name ?></a>
							</small>
						</div>
						<?php endforeach ?>
					<?php endforeach ?>
					<div style="clear: both;"></div>

					<hr class="dotted">
					<div class="slider-pocetna">
					<a class="lijevo" href="#left" data-liquidslider-ref="main-slider">
						<i class="icon-chevron-left icon-white"></i>
					</a>
					<a class="desno" href="#right" data-liquidslider-ref="main-slider">
						<i class="icon-chevron-right icon-white"></i>
					</a>
				    <div class="liquid-slider"  id="main-slider">
						<div>
						    <div class="page-header">
								<h2>
									<a href="<?php echo base_url()?>poslovni-imenik/preporucene">
										<i class="icon-flag"></i>
										<span>PreporuÄ?ene</span> firme
									</a>
								</h2>
							</div>
							    <?php foreach ($query as $q): ?>
							    <?php 
							    $parentId = $q->parentId;
							    $id = $q->id;
							    $image = $this->ListingModel->NewListingsImages($id);
							    $catSlug = $this->ListingModel->NewListingsCat($parentId);
							    ?>

						    <div class="firma1">
						      	<div class="slika">
						      		<a href="<?php echo base_url().'poslovni-imenik/firma'.$q->slug ?>">
						      			<?php if($image): ?>
						                <img src="<?php echo base_url().'uploads/thumbs/'.$image['name']; ?>" />
						                <?php else: ?>
						                <img src="<?php echo base_url() ?>images/nema-slike.jpg" />
						                <?php endif ?>
						      		</a>
						      	</div>
						      	<h5>
						      		<a href="<?php echo base_url().'poslovni-imenik/firma/'.$q->id.'/'.$q->slug ?>">
						      			<?php echo character_limiter($q->name, 27); ?>
						      		</a>
						      	</h5>
						      	<small>
						      		<i class="icon-list-alt"></i>
						      		Kategorija: <a href="<?php echo base_url().'poslovni-imenik/'.$catSlug['categorySlug'].'/'.$q->slug ?> "><?php echo $q->categoryName; ?></a>
						      	</small>
						    </div>
						    <?php endforeach ?>
						</div>
						    <div>
						      <div class="page-header">
								<h2>
									<a href="<?php echo base_url()?>poslovni-imenik/nove">
										<i class="icon-time"></i>
										<span>Nove</span> firme
									</a>
								</h2>
							</div>
							    <?php foreach ($query2 as $q): ?>
							    <?php 
							    $id = $q->id;
							    $image = $this->ListingModel->NewListingsImages($id);
							    
							    ?>

						    <div class="firma1">
						      	<div class="slika">
						      		<a href="<?php echo base_url().'poslovni-imenik/firma'.$q->slug ?>">
						      			<?php if($image): ?>
						                <img src="<?php echo base_url().'uploads/thumbs/'.$image['name']; ?>" />
						                <?php else: ?>
						                <img src="<?php echo base_url() ?>images/nema-slike.jpg" />
						                <?php endif ?>
						      		</a>
						      	</div>
						      	<h5>
						      		<a href="<?php echo base_url().'poslovni-imenik/firma/'.$q->id.'/'.$q->slug ?>">
						      			<?php echo character_limiter($q->name, 27); ?>
						      		</a>
						      	</h5>
						      	<small>
						      		<i class="icon-plus"></i>
						      		Dodano: <?php echo date('d.m.Y', strtotime($q->date)) ?>
						      	</small>
						    </div>
						    <?php endforeach ?>
						    </div>          
						    <div>
						     <div class="page-header">
								<h2>
									<a href="<?php echo base_url()?>poslovni-imenik/nove">
										<i class="icon-time"></i>
										<span>SluÄ?ajne</span> firme
									</a>
								</h2>
							</div>
							    <?php foreach ($query3 as $q): ?>
							    <?php 
							    $parentId = $q->parentId;
							    $id = $q->id;
							    $image = $this->ListingModel->NewListingsImages($id);
							    $catSlug = $this->ListingModel->NewListingsCat($parentId);
							    ?>

						    <div class="firma1">
						      	<div class="slika">
						      		<a href="<?php echo base_url().'poslovni-imenik/firma'.$q->slug ?>">
						      			<?php if($image): ?>
						                <img src="<?php echo base_url().'uploads/thumbs/'.$image['name']; ?>" />
						                <?php else: ?>
						                <img src="<?php echo base_url() ?>images/nema-slike.jpg" />
						                <?php endif ?>
						      		</a>
						      	</div>
						      	<h5>
						      		<a href="<?php echo base_url().'poslovni-imenik/firma/'.$q->id.'/'.$q->slug ?>">
						      			<?php echo character_limiter($q->name, 27); ?>
						      		</a>
						      	</h5>
						      	<small>
						      		<i class="icon-random"></i>
						      		Kategorija: <a href="<?php echo base_url().'poslovni-imenik/'.$catSlug['categorySlug'].'/'.$q->slug ?> "><?php echo $q->categoryName; ?></a>
						      	</small>
						    </div>
						    <?php endforeach ?>
						    </div>
						  </div>
				</div>
			</div>
				</div>
			<div class="span4">
				<?php $this->load->view('modules/subscription'); ?>
				<?php $this->load->view('modules/categories'); ?>
				<?php $this->load->view('modules/fb_like_box'); ?>
			</div>
		</div>
	</div>