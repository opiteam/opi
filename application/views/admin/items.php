<?php $this->load->view('admin/header'); ?>
<div id="sadrzaj">
	<div id="sadrzaj_block">
	<div id="head_bg">
		<div id="head_txt">Categories</div>
		<div id="add_btn">
			<a href="<?php echo base_url('admin/listing/addItem')?>">Add Listing</a>
		</div>
		<div id="add_btn">
			<a href="<?php echo base_url('admin/listing/addCategory')?>">Add Category</a>
		</div>
	</div>
	<div id="lista">
		<div id="naslov">Category Name</div>
		<div id="line"></div>
		<?php foreach($query as $category) : ?>
			<div id="item">
				<a href="<?php echo base_url('admin/listing/category/'.$category->id);?>"><?php echo $category->categoryName; ?></a>
				<a id="obrisi_kat" class="obrisi" href="#" title="obriši" data-id="<?php echo $category->id ?>"></a>
				<a class="uredi" title="uredi kategoriju" href="<?php echo base_url('admin/listing/editCategory/'.$category->id)?>"></a>
			</div>
		<?php endforeach ?>
		<div style="clear:both;"></div>
	</div>
	<?php echo $link ?>
</div>
<?php $this->load->view('admin/footer'); ?>