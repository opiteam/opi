<?php $this->load->view('admin/header'); ?>
<div id="sadrzaj">
	<div id="sadrzaj_block">
	<div id="head_bg">
		<?php foreach ($cat as $c): ?>
    		<div id="head_txt"><?php echo $c->categoryName ?></div>
    	<?php endforeach ?>
      	<div id="add_btn">
			<a href="<?php echo base_url('admin/listing/addSubcategory/'.$this->uri->segment(4))?>">Add Subcategory</a>
		</div>
	</div>
	<div id="lista">
		<div id="naslov">Category Name</div>
		<div id="line"></div>
<?php foreach ($query as $subcat): ?>
	<div id="item">
		<a href="<?php echo base_url('admin/listing/subcategory/').$subcat->id ?>"><?php echo $subcat->categoryName ?></a>
		<a id="obrisi_podkat" class="obrisi" title="obriši" href="#" data-id="<?php echo $subcat->id ?>"></a>
		<a class="uredi" title="uredi podkategoriju" href="<?php echo base_url('admin/listing/editSubcategory/'.$subcat->id);?>"></a>
	</div>
<?php endforeach ?>
    </div>
    <?php echo $link ?>
</div>

<?php $this->load->view('admin/footer'); ?>