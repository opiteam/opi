<?php $this->load->view('admin/header'); ?>
<div id="sadrzaj">
	<div id="sadrzaj_block">
	<div id="head_bg">
		<div id="head_txt">Kategorije</div>
		<div id="add_btn">
			<a href="<?php echo base_url()?>admin/vijesti/dodaj-vijesti">Dodaj vijest</a>
		</div>
		<div id="add_btn">
			<a href="<?php echo base_url()?>admin/vijesti/dodaj-kategoriju">Dodaj Kategoriju</a>
		</div>
	</div>
	<div id="lista">
		<div id="naslov">Ime kategorije</div>
		<div id="line"></div>
		<?php foreach($kategorije as $kategorija) : ?>
			<div id="item">
				<a href="<?php echo base_url();?>admin/vijesti/kategorija/<?php echo $kategorija->id ?>"><?php echo $kategorija->Ime_kategorije; ?></a>
				<a id="obrisi_vijestikat" class="obrisi" href="#" title="obriši" data-id="<?php echo $kategorija->id ?>"></a>
				<a class="uredi" title="uredi kategoriju" href="<?php echo base_url();?>admin/vijesti/uredi-kategoriju/<?php echo $kategorija->id ?>"></a>
			</div>
		<?php endforeach ?>
		<div style="clear:both;"></div>
	</div>
	<?php echo $link ?>
</div>
<?php $this->load->view('admin/footer'); ?>