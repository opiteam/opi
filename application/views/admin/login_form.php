<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/login.css" />
<title>Opi.ba - Prijava Administratora</title>
</head>
<body>
<div id="login_form">
<h1>Prijava</h1>
<div class="notif"><div class="notif_ico"></div>U polja unesite podatke za prijavu u admin sekciju</div>
<?php 
     echo form_open('login/validation');
     echo form_label('Korisničko ime', 'username');
     echo form_input('username');
     echo form_label('Password', 'password');
     echo form_password('password');
	 echo form_submit('submit', 'Prijava');
	 
	 echo form_close();
?>

<?php echo validation_errors('<div class="error">'); ?>

</div>
</body>
</html>