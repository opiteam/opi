<div id="sidebar">
	<?php $username = $this->session->userdata('username'); ?>
	<a id="logo" href="<?php echo base_url('admin');?>"></a>
	<div id="profile_data">
		Welcome <?php echo $username; ?>
		<div style="clear:both;"></div>
		<a href="<?php echo base_url();?>" target="_blank">View website</a> | <a href="<?php echo base_url('admin/dashboard/logout')?>">Logout</a>
	</div>
	<div id="menu">
		<div class="menu_item <?=($this->uri->segment(2)==='index')?'active':''?>"><a href="<?php echo base_url('admin/dashboard/index');?>">Dashboard</a></div>
		<div class="menu_item <?=($this->uri->segment(2)==='listing')?'active':''?>"><a href="<?php echo base_url('admin/listing');?>">Listings</div>
		<div class="menu_item <?=($this->uri->segment(2)==='media-manager')?'active':''?>"><a href="<?php echo base_url();?>admin/media-manager">Upload image</a></div>
		<div class="menu_item <?=($this->uri->segment(2)==='vijesti')?'active':''?>"><a href="<?php echo base_url();?>admin/vijesti">Business articles</a></div>
		<div class="menu_item <?=($this->uri->segment(2)==='custom-page')?'active':''?>"><a href="<?php echo base_url();?>admin/custom-pages">Custom pages</a></div>
		<div class="menu_item <?=($this->uri->segment(2)==='menu')?'active':''?>"><a href="<?php echo base_url();?>admin/menu">Menu</a></div>
		<?php if ($this->session->userdata('tip') == 1): ?>
			<div class="menu_item <?=($this->uri->segment(2)==='users')?'active':''?>"><a href="<?php echo base_url();?>admin/users">Users</a></div>
		<?php endif ?>
		<div class="menu_item <?=($this->uri->segment(2)==='configuration')?'active':''?>"><a href="<?php echo base_url();?>admin/configuration">Configuration</a></div>

	</div>
</div>