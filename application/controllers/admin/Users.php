<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {
    
    function index() {
        $this->load->library("pagination");
        $config = array();
        $config["base_url"] = base_url('admin/users/index');
        $config["total_rows"] = $this->AdminModel->usersCount();
        $config["per_page"] = 5;
        $config["uri_segment"] = 4;
        $config['last_link'] = 'Last';
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $data = array(
            'title' => "Users",
            'query' => $this->AdminModel->allUsers($config["per_page"], $page),
            'link' =>$this->pagination->create_links()
            );

        $this->load->view('admin/users', $data);
    }
    
    function create() {
        $data = array(
            'title' => "Add User",
            );
        $this->load->library('form_validation');
        $this->load->helper('security');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        $this->form_validation->set_rules('name', 'Username', 'trim|required|strip_tags|xss_clean');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|strip_tags|xss_clean');
        $this->form_validation->set_rules('password_confirm', 'Password Confirmation', 'trim|required|matches[password]|strip_tags|xss_clean');

        if ($this->form_validation->run() === FALSE)
            {       
                $this->load->view('admin/addUser', $data);
                        
            }
        else
            {

                $this->AdminModel->addUser();
                $id = $this->db->insert_id();
                $this->session->set_flashdata('notification', 'User successfully created');
                redirect('admin/users/edit/'.$id);
                
            }
    }

    function edit($id) {
        $data = array(
            'title' => "Edit user",
            'query' => $this->AdminModel->user($id)
            );
        $this->load->library('form_validation');
        $this->load->helper('security');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        $this->form_validation->set_rules('name', 'Username', 'trim|required|strip_tags|xss_clean');
        $this->form_validation->set_rules('password', 'Password', 'trim|strip_tags|xss_clean');
        $this->form_validation->set_rules('password_confirm', 'Password Confirmation', 'trim|matches[password]|strip_tags|xss_clean');

        if ($this->form_validation->run() === FALSE)
            {       
                $this->load->view('admin/editUser', $data);
                        
            }
        else
            {

                $this->AdminModel->editUser();
                $this->session->set_flashdata('notification', 'User successfully edited');
                redirect('admin/users');
                
            }
    }
}