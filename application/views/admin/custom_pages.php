<?php $this->load->view('admin/header'); ?>
<div id="sadrzaj">
	<div id="sadrzaj_block">
	<div id="head_bg">
		<div id="head_txt">Custom pages</div>
		<div id="add_btn">
			<a href="<?php echo base_url()?>admin/custom-pages/add-new">Add new custom page</a>
		</div>
	</div>
	<div id="lista">
		<div id="naslov">Title</div>
		<div id="line"></div>
		
		<?php foreach($query as $q) : ?>
			<div id="item">
				<a href="<?php echo base_url();?>admin/custom-pages/edit-page/<?php echo $q->PageId ?>"><?php echo $q->PageTitle; ?></a>
				<a id="removeNewsItem" class="obrisi" href="#" title="obriši" data-id="<?php echo $q->PageId ?>"></a>
				<div class="autor">Created: <strong><?php echo $q->PageDate ?></strong></div>
			</div>
		<?php endforeach ?>
		<div style="clear:both;"></div>
	</div>
	<?php echo $link ?>
</div>
<?php $this->load->view('admin/footer'); ?>