<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
        function __construct()
	{	
            parent::__construct();
		
            $session_data = $this->session->userdata('logged_in');
            $data['username'] = $session_data['username'];	
	}
        
	public function index()
	{
            $data = array(
                'title' => 'Home',
                'description' => "Najveca Online baza firmi u BiH te najnovije vijesti iz svijeta biznisa",
                'image' => base_url() . "images/logo_default.png",
                'query' => $this->ListingModel->RecomendedListingsHome(),
                'query2' => $this->ListingModel->NewListingsHome(),
                'query3' => $this->ListingModel->RandomListingsHome(),
                'query6' => $this->NewsModel->NewsHome(),
                'query7' => $this->ListingModel->CategoriesHome(),
                'query8' => $this->NewsModel->LastNewsHomeCat(),
                'configuration' => $this->AdminModel->get_configuration()
        );
            
            $this->load->view('home/index', $data);
            $this->load->view('footer');
	}
}