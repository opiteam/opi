<?php $this->load->view('admin/header'); ?>
<style>
.nav-list>li>a {
padding: 0px 15px !important;
}
</style>
<div id="sadrzaj">
  <div id="sadrzaj_block">
	<div id="head_bg">
		<div id="head_txt">Choose</div>
	</div>
	<div id="lista">
    <div style="clear: both;"></div>
		<div class="icon">
	     <?php $id_next = $this->db->insert_id(); ?>
    		<a id="firm_img" href="<?php echo base_url('admin/listing/add_listing/'.$id_next)?>"></a>
    		<div style="clear:both;"></div>
    		<div class="add_icon">Add Listing</div>
		</div>

		<div class="icon">
    		<a id="prom_img" href="<?php echo base_url('admin/news/addItem');?>"></a>
		<div class="add_icon">Add News item</div>
	
		</div>
    <div style="clear: both;"></div>
	</div>
</div>


  <div id="sadrzaj_block">
  <div id="head_bg">
    <div id="head_txt">Statistics</div>
  </div>
  <div id="lista">
    <div style="clear: both;"></div>
    Listing count: <strong><?php echo $itemsCount ?></strong>
    <table class="table table-striped">
      <tr>
        <td>Listings added by</td>
        <td>Last Listing Items</td>
        <td>Last News Items</td>
      </tr>
      <tr>
        <td>
          <ul class="nav nav-list">
          <?php foreach ($users as $user): ?>
          <?php $id = $user->id; ?>
          <li><?php echo $user->username ?> added <strong> <?php echo $this->AdminModel->listingNumberPerAuthor($id) ?> </strong> listings</li>
          <li class="divider"></li>
          <?php endforeach ?>
          <h6><a style="float: right;" href="<?php echo base_url('admin/users')?>">Users</a></h6>
          </ul>
        </td>
        <td>
        <ul class="nav nav-list">
        <?php foreach ($lastItems as $zf): ?>
          <li><a href="<?php if ($zf->approved == 0) {echo base_url('admin/listing/approveItem/'.$zf->id);} else {echo base_url('admin/listing/editItem/'.$zf->id);} ?>" title="Edit Listing"><?php echo character_limiter($zf->name, 20) ?></a></li>
          <li class="divider"></li>
        <?php endforeach ?>
        <h6><a style="float: right;" href="<?php echo base_url('admin/listing/allItems')?>">All Items</a></h6>
        </ul>
        </td>
        <td>
        <ul class="nav nav-list">
        <?php foreach ($lastNews as $zv): ?>
          <li><a href="<?php echo base_url('admin/news/editItem/'.$zf->id) ?>" title="Edit News Item"><?php echo character_limiter($zv->title, 20) ?></a></li>
          <li class="divider"></li>        
        <?php endforeach ?>
        <h6><a style="float: right;" href="<?php echo base_url('admin/news/')?>">All News</a></h6>
        </ul>
        </td>
      </tr>
    </table>
    <div style="clear: both;"></div>
  </div>
</div>
</div>


<?php $this->load->view('admin/footer'); ?>