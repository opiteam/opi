<aside>
	<div class="widget pretplata">
		<h2>Pretplatite se na naše novosti</h2>
		<p>Unesite u polje Vašu E-mail adresu kako bi redovno primali Novosti sa Opi.ba portala</p>
		<?php echo form_open('site/pip_email'); ?>
		<?php echo validation_errors('<div class="error">'); ?>
		<?php if( isset( $error) ) echo $error; ?>
		<div class="input-prepend input-append">
			<span class="add-on">
				<i class="icon-envelope"></i>
			</span>
			<input class="span2" style="height: 30px" type="text" name="email_pretplata" placeholder="Unesite Vaš E-mail" />
			<input type="submit" value="Pošalji" class="add-on" style="height: 30px"/>
		</div>
			<?php echo form_close(); ?>
	</div>
</aside>