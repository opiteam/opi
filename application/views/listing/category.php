<?php $this->load->view('site/header'); ?>
<div class="main">
	<div class="container">
		<div class="row">
			<div class="span8">
				<div class="content">
					<div class="page-header">
						<h1><?php echo $kat_ime['Ime_kategorije'] ?></h1>
					</div>
					<div class="page">
						<div class="podkat">
								<div class="well well-small">
									<ul>
									<?php foreach ($query as $q): ?>
										<?php $id = $q->id; ?>
											<li><h5><a href="<?php echo base_url().'poslovni-imenik/'.$kat_ime['link_kategorije'].'/'.$q->link_kategorije ?>"><?php echo $q->Ime_kategorije ?><span class="bubble"><?php echo $this->site_model->broj_firmi_popodkat($id) ?></span></a></h5></li>
									<?php endforeach ?>
									</ul>
									<div style="clear: both"></div>
								</div>
						</div>
					</div>
				</div>
			</div>
			<div class="span4">
				<?php $this->load->view('site/moduli/tabovi_firme'); ?>
			</div>
		</div>
	</div>
<?php $this->load->view('site/footer'); ?>
 




	
