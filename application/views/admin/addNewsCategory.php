<?php $this->load->view('admin/header'); ?>
<div id="sadrzaj">
	<div id="sadrzaj_block">
		<div id="head_bg">
        	<div id="head_txt">Dodaj kategoriju</div>
    	</div> 
		<div id="lista">
			<div id="line"></div>
			<?php echo form_open('admin/addNewsCategory'); ?>
			<?php echo validation_errors('<div class="error">'); ?>
			<table width="600">
			<tr>
			<td valign="top">
			<fieldset>
				<legend>Podaci o kategoriji</legend>
			<label>Ime Kategorije:</label>
			<?php echo form_input('ime_kategorije'); ?>
			</fieldset>
			</td>
			</tr>
			</table>
			<?php echo form_submit('submit', 'Dodaj'); ?>
			<?php echo form_close(); ?>
			<div style="clear:both"></div>
		</div>
	</div>

<?php $this->load->view('admin/footer'); ?>