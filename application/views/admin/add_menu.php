<?php $this->load->view('admin/header'); ?>
<div id="sadrzaj">
	<div id="sadrzaj_block">
	<div id="head_bg">
		<div id="head_txt">Add new menu</div>
	</div>
	<div id="lista">
    <div id="line"></div>
<?php echo form_open('admin/add_menu'); ?>
<?php echo validation_errors('<div class="error">'); ?>
<?php if( isset( $error) ) echo $error; ?>
<table width="600">
			<tr>
			<td valign="top">
			<fieldset>
				<legend>Menu item details</legend>
				<label>Menu Name</label>
				<?php echo form_input('name'); ?>
				<label>Choose what to link</label>
				<select name="ChooseLink" id="choose">
					<option value="listings">Listings</option>
					<option value="custom_page">Custom page</option>
					<option value="custom_link">Custom link</option>
				</select>
				<div id="appear_option"></div>
				<label>Parent menu</label>
				<select name="parent">
					<option value="primjer">primjer</option>
				</select>
			</fieldset>
			</td>
			</tr>
			</table>
<?php echo form_submit('submit', 'Add menu'); ?>
<?php echo form_close(); ?>
<div style="clear:both"></div>

</div>
</div>

<script>
$("#choose").change(function() {
	var choose = $("#choose").val();
	

	if (choose == "custom_page") {
		$('#appear_option').html('<label>Choose page</label><select name="link"><option value="categories">Categories</option><option value="category">Category</option></select>');
	}

	if (choose == "listings") {
		$('#appear_option').html('<label>Choose option</label><select name="link"><option value="categories">Categories</option><option value="category">Category</option></select>');
	}
	
})
</script>
<?php $this->load->view('admin/footer'); ?>
