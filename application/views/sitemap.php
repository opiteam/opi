<?= '<?xml version="1.0" encoding="UTF-8" ?>' ?>

<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    <url>
        <loc><?= base_url();?></loc> 
        <priority>1.0</priority>
    </url>

    <!-- My code is looking quite different, but the principle is similar -->
    <?php foreach($firme_kat as $urlk) { ?>
    <?php if ($urlk->id_nosece == 0): ?>
    <url>
        <loc><?= base_url().'poslovni-imenik/'.$urlk->link_kategorije ?> ?></loc>
        <priority>0.5</priority>
    </url>
    <?php endif ?>
    <?php } ?>
    <?php foreach($firme as $url) { ?>
    <url>
        <loc><?= base_url().'poslovni-imenik/item/'.$url->id.'/'.$url->link ?></loc>
        <priority>0.5</priority>
    </url>
    <?php } ?>
    <?php foreach($vijesti_kat as $vijk) { ?>
    <url>
        <loc><?= base_url().'poslovne-novosti/'.$vijk->link_kategorije ?></loc>
        <priority>0.5</priority>
    </url>
    <?php } ?>
    <?php foreach($vijesti as $vij) { ?>
    <url>
        <loc><?= base_url().'poslovne-novosti/vijest/'.$vij->id.'/'.$vij->link ?></loc>
        <priority>0.5</priority>
    </url>
    <?php } ?>
</urlset>