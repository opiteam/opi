<?php

Class AdminModel extends CI_Model 
{

   function login($username, $password)
   {
   $this->db->select('id, username, password, type');
   $this->db->from('users');
   $this->db->where('username', $username);
   $this->db->where('password', sha1(salt.$password));
   $this->db->limit(1);

   $query = $this->db->get();

   if($query->num_rows() == 1)
   {
     return $query->result();
   }
   else
   {
     return false;
   }
  }

  function getListings($id, $limit, $start) {
    $this->db->limit($limit, $start);
    $this->db->select('listings.id, listings.name, listings.date, listings.approved, listings.author, users.username');
    $this->db->from('listings');
    $this->db->join('items_categories', 'listings.id = items_categories.listingId');
    $this->db->where('items_categories.categoryId', $id);
    $this->db->join('users', 'listings.author = users.id');
    $this->db->order_by('date', 'DESC');
    $query = $this->db->get();
    return $query->result();
  }

  function item($id) {
    $this->db->where('id', $id);
    $query = $this->db->get('listings');
    return $query->row_array();
  }

  function currentSubcategory($id) {
    $this->db->select('categories.categoryName, categories.id, items_categories.categoryId');
    $this->db->from('categories');
    $this->db->where('items_categories.listingId', $id);
    $this->db->join('items_categories', 'categories.id = items_categories.categoryId');
    $query = $this->db->get();
    return $query->row_array();
  }

  function categories($limit, $start) {
    $this->db->limit($limit, $start);
    $this->db->select();
    $this->db->from('categories');
    $this->db->where('parentId', 0);
    $this->db->order_by('categoryName', 'ASC');
    $query = $this->db->get();
    return $query->result();
  }

  function categoriesCount() {
    $this->db->where('parentId', 0);
    $query = $this->db->get("categories");
    return $query->num_rows(); 
  }

  function itemsCount() {
    return $this->db->count_all("listings");
  }

 function listingNumberPerAuthor($id) {
    $this->db->where('author', $id);
    $query = $this->db->get("listings");
    return $query->num_rows();
  }

  function usersCount() {
    return $this->db->count_all("users");
  }

  function users() {
    $query = $this->db->get('users');
    return $query->result();
  }

   function allUsers($limit, $start) {
    $this->db->limit($limit, $start);
    $query = $this->db->get('users');
    return $query->result();
  }

  function NewsCount() {
    return $this->db->count_all("news");
  }
   function page_count() {
    return $this->db->count_all("news");
  }

  function NewsCategoriesCount() {
    return $this->db->count_all("newscategories");
  }

  function subcategoriesCount($idn) {
    $this->db->where('parentId', $idn);
    $query = $this->db->get("categories");
    return $query->num_rows();
  }

function NewsPerCategoryCount($id) {
    $this->db->where('categoryId', $id);
    $query = $this->db->get("news");
    return $query->num_rows(); 
  }

  function itemsCountPerCategory($id) {
    $this->db->select();
    $this->db->from('listings');
    $this->db->join('listings_categories', 'listingId = listings.id');
    $this->db->where('listings_categories.categoryId', $id);
    $query = $this->db->get();
    return $query->num_rows(); 
  }

  function image($id) {
    $this->db->select();
    $this->db->from('images');
    $this->db->where('itemId', $id);
    $query = $this->db->get();
    return $query->result();
  }

  function subcategoryList() {
    $this->db->order_by("categoryName", "asc");
    $query = $this->db->get('categories');
    return $query->row_array();
  }

  function subcategories($id, $limit, $start) {
    $this->db->limit($limit, $start);
    $this->db->select('categories.categoryName, categories.id');
    $this->db->from('categories');
    $this->db->where('parentId', $id);
    $query = $this->db->get();
    return $query->result();
  }

  function addCategory($data2) {
    $link = url_title(convert_accented_characters($this->input->post('name')), 'dash', TRUE);
    $data = array(
      'categoryName' => $this->input->post('name'),
      'categorylug' => $link,
      'image' => $data2
      );

    $this->db->insert('categories', $data);
  }

  function addItem() {
    $this->load->helper('url');
    $link = url_title(convert_accented_characters($this->input->post('name')), 'dash', TRUE);
    $datum = strtotime($this->input->post('deadline'));
    $ndatum = date('Y-m-d', $datum);
    $data = array(
      'name' => $this->input->post('name'),
      'slug' => $link,
      'author' => $this->session->userdata('id'),
      'description' => $this->input->post('description'),
      'address' => $this->input->post('address'),
      'zip' => $this->input->post('zip'),
      'city' => $this->input->post('city'),
      'state' => $this->input->post('state'),
      'country' => $this->input->post('country'),
      'phone1' => $this->input->post('phone1'),
      'phone2' => $this->input->post('phone2'),
      'phone3' => $this->input->post('phone3'),
      'phone4' => $this->input->post('phone4'),
      'fax' => $this->input->post('fax'),
      'Email1' => $this->input->post('email1'),
      'Email2' => $this->input->post('email2'),
      'web' => $this->input->post('web'),
      'lat' => $this->input->post('lat'),
      'lng' => $this->input->post('lng'),
      'zoom' => $this->input->post('zoom'),
      'date' => date("Y-m-d H:i:s"),
      'approved' => 1
      ); 

    $this->db->insert('listings', $data);
  }


  function imagesItems($id, $images) {
    $count = 1;
    foreach ($images as $image) {
    $data = array(
      'listingId' => $id,
      'name' => $image['file_name'],
      'first' => $count++
      );
    
    $this->db->insert('images', $data);
  }
  }

  function itemsCategories($id) {
    $data = array(
      'listingId' => $id,
      'categoryId' => $this->input->post('category')
      );
    
    $this->db->insert('listings_categories', $data);
  }

  function category($id) {
    $this->db->where('id', $id);
    $query = $this->db->get('categories');
    return $query->result();
  }

   function subcategory($id) {
    $this->db->where('id', $id);
    $query = $this->db->get('categories');
    return $query->result();
  }

  function addSubcategory() {
    $data = array(
      'categoryName' => $this->input->post('subcategoryName'),
      'parentId' => $this->input->post('parentId')
      );

    $this->db->insert('categories', $data);
  }

  function editItem() {
    $this->load->helper('url');
    $link = url_title(convert_accented_characters($this->input->post('name')), 'dash', TRUE);
    $data = array(
      'name' => $this->input->post('name'),
      'slug' => $link,
      'author' => $this->session->userdata('id'),
      'description' => $this->input->post('description'),
      'address' => $this->input->post('address'),
      'zip' => $this->input->post('zip'),
      'city' => $this->input->post('city'),
      'state' => $this->input->post('state'),
      'country' => $this->input->post('country'),
      'phone1' => $this->input->post('phone1'),
      'phone2' => $this->input->post('phone2'),
      'phone3' => $this->input->post('phone3'),
      'phone4' => $this->input->post('phone4'),
      'fax' => $this->input->post('fax'),
      'Email1' => $this->input->post('email1'),
      'Email2' => $this->input->post('email2'),
      'web' => $this->input->post('web'),
      'lat' => $this->input->post('lat'),
      'lng' => $this->input->post('lng'),
      'zoom' => $this->input->post('zoom')
      );

    $this->db->where('id', $this->input->post('id'));
    $this->db->update('listings', $data);
  }

  function approveItem() {
    $this->load->helper('url');
    $link = url_title(convert_accented_characters($this->input->post('name')), 'dash', TRUE);
    $data = array(
      'name' => $this->input->post('name'),
      'slug' => $link,
      'author' => $this->session->userdata('id'),
      'description' => $this->input->post('description'),
      'address' => $this->input->post('address'),
      'zip' => $this->input->post('zip'),
      'city' => $this->input->post('city'),
      'state' => $this->input->post('state'),
      'country' => $this->input->post('country'),
      'phone1' => $this->input->post('phone1'),
      'phone2' => $this->input->post('phone2'),
      'phone3' => $this->input->post('phone3'),
      'phone4' => $this->input->post('phone4'),
      'fax' => $this->input->post('fax'),
      'Email1' => $this->input->post('email1'),
      'Email2' => $this->input->post('email2'),
      'web' => $this->input->post('web'),
      'lat' => $this->input->post('lat'),
      'lng' => $this->input->post('lng'),
      'zoom' => $this->input->post('zoom'),
      'date' => date("Y-m-d H:i:s"),
      'approved' => 1
      );

    $this->db->where('id', $this->input->post('id'));
    $this->db->update('listings', $data);
  }

  function editItemsCategories() {
    $data = array(
      'categoryId' => $this->input->post('category')
      );
    $this->db->where('listingId', $this->input->post('id'));
    $this->db->update('listings_categories', $data);
  }

  function editCategory() {
    $this->load->helper('url');
    $link = url_title(convert_accented_characters($this->input->post('name')), 'dash', TRUE);
    $data = array(
      'categorySlug' => $link,
      'categoryName' => $this->input->post('name')
      );

    $this->db->where('id', $this->input->post('id'));
    $this->db->update('categories', $data);
  }

  function editSubcategory() {
    $this->load->helper('url');
    $link = url_title(convert_accented_characters($this->input->post('subcategoryName')), 'dash', TRUE);
    $data = array(
      'categorySlug' => $link,
      'categoryName' => $this->input->post('subcategoryName')
      );

    $this->db->where('id', $this->input->post('id'));
    $this->db->update('categories', $data);
  }

  function removeCategory($id)
  {
    $this->db->where('id', $id);
    $this->db->delete('categories');
  }

  function removeNewsItem($id)
  {
    $this->db->where('id', $id);
    $this->db->delete('news');
  }

  function removeUser($id)
  {
    $this->db->where('id', $id);
    $this->db->delete('users');
  }

  function removeNewsCategory($id)
  {
    $this->db->where('id', $id);
    $this->db->delete('newscategories');
  }

  function removeSubcategory($id)
  {
    $this->db->where('id', $id);
    $this->db->delete('categories');
  }
  
  function removeItemsCategories($id)
  {
    $this->db->where('id', $id);
    $this->db->delete('listings_categories');
  }

  function removeItem($id)
  {
    $this->db->where('id', $id);
    $this->db->delete('listings');
  }

  function removeImage($id)
  {
    $this->db->where('id', $id);
    $this->db->delete('images');
  }

  function itemAuthor($id) {
    $this->db->select('users.username', 'listings.id');
    $this->db->from('users');
    $this->db->where('listings.id', $id);
    $this->db->join('listings', 'users.id = listings.autor');
    $query = $this->db->get();
    return $query->result();
  }

  function addNewsCategory() {
    $this->load->helper('url');
    $link = url_title(convert_accented_characters($this->input->post('name')), 'dash', TRUE);
    $data = array(
      'categorySlug' => $link,
      'categoryName' => $this->input->post('name')
      );

    $this->db->insert('newscategories', $data);
  }

  function NewsCat() {
    $query = $this->db->get('newscategories');
    return $query->result();
  }

  function categoryForNews($id) {
    $this->db->where('id', $id);
    $query = $this->db->get('newscategories');
    return $query->result();
  }

  function AddNewsItem() {
    $this->load->helper('url');
    $link = url_title(convert_accented_characters($this->input->post('title')), 'dash', TRUE);
    $data = array(
      'slug' => $link,
      'title' => $this->input->post('title'),
      'categoryId' => $this->input->post('category'),
      'content' => $this->input->post('content'),
      'date' => date("Y-m-d H:i:s")
      );
    $this->db->insert('news', $data);
  }

  function editNewsItem() {
    $this->load->helper('url');
    $link = url_title(convert_accented_characters($this->input->post('naslov')), 'dash', TRUE);
    $data = array(
      'slug' => $link,
      'title' => $this->input->post('title'),
      'categoryId' => $this->input->post('category'),
      'content' => $this->input->post('content'),
      'date' => date("Y-m-d H:i:s")
      );
    $this->db->where('id', $this->input->post('id'));
    $this->db->update('news', $data);
  }

  function news($limit, $start) {
    $this->db->limit($limit, $start);
    $this->db->select('news.date, news.id, news.title, newscategories.name');
    $this->db->from('news');
    $this->db->join('newscategories', 'newscategories.id = news.categoryId');
    $this->db->order_by('news.date', 'desc');
    $query = $this->db->get();
    return $query->result();
  }

  function newsPerCategory($id, $limit, $start) {
    $this->db->limit($limit, $start);
    $this->db->select('news.categoryId, news.title, news.id, newscategories.name');
    $this->db->from('news');
    $this->db->where('categoryId', $id);
    $this->db->join('newscategories', 'newscategories.id = news.categoryId');
    $query = $this->db->get();
    return $query->result();
  }

  function newsItem($id) {
    $this->db->select('news.id, vijesti.categoryId, news.title, news.content, newscategories.name');
    $this->db->from('news');
    $this->db->where('news.id', $id);
    $this->db->join('newscategories', 'newscategories.id = news.categoryId');
    $query = $this->db->get();
    return $query->result();
  }

  function currentNewsCategory($id) {
    $this->db->select('podkategorije.Ime, podkategorije.id, firme.kategorija');
    $this->db->from('podkategorije');
    $this->db->where('firme.id', $id);
    $this->db->join('firme', 'podkategorije.id = firme.kategorija');
    $query = $this->db->get();
    return $query->result();
  }

  function NewsCategories($limit, $start) {
    $this->db->limit($limit, $start);
    $query = $this->db->get('newscategories');
    return $query->result();
  }

  function editNewsCategory() {
    $data = array(
      'name' => $this->input->post('name')
      );
    $this->db->where('id', $this->input->post('id'));
    $this->db->update('newscategories', $data);
  }

  function addUser() {
    $data = array(
      'username' => $this->input->post('name'),
      'password' => sha1(salt.$this->input->post('password')),
      'type' => $this->input->post('type')
      );
    $this->db->insert('users', $data);
  }

  function user($id) {
    $this->db->where('id', $id);
    $query = $this->db->get('users');
    return $query->result();
  }

  function editUser() {
    $data = array(
      'username' => $this->input->post('name'),
      'password' => sha1(salt.$this->input->post('password')),
      'type' => $this->input->post('type')
      );
    $this->db->where('id', $this->input->post('id'));
    $this->db->update('users', $data);
  }

  function lastItems() {
    $this->db->limit(4);
    $this->db->order_by("date", "desc");
    $query = $this->db->get('listings');
    return $query->result();
  }

  function allItems($limit, $start) {
    $this->db->limit($limit, $start);
    $this->db->select('listings.id, listings.name, listings.date, listings.approved, listings.author, users.username');
    $this->db->from('listings');
    $this->db->join('users', 'listings.author = users.id');
    $this->db->order_by("date", "desc");
    $query = $this->db->get();
    return $query->result();
  }

  function lastNews() {
    $this->db->limit(4);
    $this->db->order_by("date", "desc");
    $query = $this->db->get('news');
    return $query->result();
  }

  function configuration($data2) {
    $data = array(
      'SiteName' => $this->input->post('site_name'),
      'SiteDesc' => $this->input->post('site_desc'),
      'Logo' => $data2,
      'ShowPublished' => $this->input->post('show_published')
      );
    $this->db->update('configuration', $data);
  }

  function get_configuration() {
    $query = $this->db->get('configuration');
    return $query->row_array();

  }

  function add_custom_page() {
    $this->load->helper('url');
    $link = url_title(convert_accented_characters($this->input->post('title')), 'dash', TRUE);
    $data = array(
      'PageTitle' => $this->input->post('title'),
      'PageLink' => $link,
      'PageContent' => $this->input->post('content'),
      'PageDate' => date("Y-m-d H:i:s")
      );

    $this->db->insert('custom_pages', $data);
  }

  function custom_pages() {
    $query = $this->db->get('custom_pages');
    return $query->result();
  }
}




