<?php $this->load->view('admin/header'); ?>
<div id="sadrzaj">
	<div id="sadrzaj_block">
	<div id="head_bg">
    		<div id="head_txt">Users</div>
      	<div id="add_btn">
			<a href="<?php echo base_url('admin/users/create')?>">Add User</a>
		</div>
	</div>
	<div id="lista">
		<div id="naslov">Username</div>
		<div id="line"></div>
<?php foreach ($query as $q): ?>
	<div id="item">
		<a href="<?php echo base_url('admin/users/edit/'.$q->id) ?>"><?php echo $q->username ?></a>
		<a id="removeUser" class="obrisi" title="obriši" href="#" data-id="<?php echo $q->id ?>"></a>
	</div>
<?php endforeach ?>
    </div>
    <?php echo $link ?>
</div>

<?php $this->load->view('admin/footer'); ?>