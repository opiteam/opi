<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Listing extends CI_Controller {

	public function index()
	{
            $data = array(
                'query' => $this->ListingModel->categories(),
                'image' => base_url() . "images/logo_default.png",
                'title' => "Directory",
                'opis' => "Najveca Online baza firmi u BiH te najnovije vijesti iz svijeta biznisa",
                'brojac' => $this->AdminModel->itemsCount(),
                'query2' => $this->ListingModel->ListingsRecomended(),
                'query3' => $this->ListingModel->NewListings(),
                'query4' => $this->ListingModel->RandomListings()
            );
            $this->load->view('listing/index', $data);
	}
        
        function category($link) {
		$data = array(
			'query' => $this->site_model->category($link),
			'opis' => "Najveca Online baza firmi u BiH te najnovije vijesti iz svijeta biznisa",
			'slika' => base_url()."images/logo_default.png",
			'kat_ime' => $this->ListingModel->kategorija_ime($link),
			'query2' => $this->ListingModel->prep_firme(),
			'query3' => $this->ListingModel->nove_firme(),
			'query4' => $this->ListingModel->sluc_firme()
			);
		$data['title'] = $data['kat_ime']['Ime_kategorije'];
		$this->load->view('listing/category', $data);
	}
        
        function subcategory() {
		$link = $this->uri->segment(3);
		$this->load->library("pagination");
		$config['per_page'] = 10;
		$config["base_url"] = base_url()."poslovni-imenik/".$this->uri->segment(2).'/'.$link;
		$config['uri_segment'] = 4;
		$config['num_links'] = 9;
		$config['total_rows'] = $this->ListingModel->podkategorija_count($link);
		$config['full_tag_open'] = '<div class="pagination"><ul>';
		$config['full_tag_close'] = '</ul></div><!--pagination-->';
		 
		$config['first_link'] = '&laquo; Prva';
		$config['first_tag_open'] = '<li class="prev page">';
		$config['first_tag_close'] = '</li>';
		 
		$config['last_link'] = 'Zadnja &raquo;';
		$config['last_tag_open'] = '<li class="next page">';
		$config['last_tag_close'] = '</li>';
		 
		$config['next_link'] = 'Slijedeća &rarr;';
		$config['next_tag_open'] = '<li class="next page">';
		$config['next_tag_close'] = '</li>';
		 
		$config['prev_link'] = '&larr; Prethodna';
		$config['prev_tag_open'] = '<li class="prev page">';
		$config['prev_tag_close'] = '</li>';
		 
		$config['cur_tag_open'] = '<li class="active"><a href="">';
		$config['cur_tag_close'] = '</a></li>';
		 
		$config['num_tag_open'] = '<li class="page">';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
		$data = array(
			'query' => $this->ListingModel->subcategory($link, $config["per_page"], $page),
			'link' => $this->pagination->create_links(),
			'opis' => "Najveća Online baza firmi u BiH te najnovije vijesti iz svijeta biznisa",
			'slika' => base_url()."images/logo_default.png",
			'kat_ime' => $this->ListingModel->podkategorija_ime($link),
			'query2' => $this->ListingModel->prep_firme(),
			'query3' => $this->ListingModel->nove_firme(),
			'query4' => $this->ListingModel->sluc_firme()
			);

		$data['title'] = $data['kat_ime']['Ime_kategorije'];

  		$this->load->view('listing/subcategory', $data);
        }
        
        function item($link) {
		$link = $this->uri->segment(4);
		$data = array(
			'query' => $this->ListingModel->item($link),
			'images' => $this->ListingModel->slike($link),
			'query2' => $this->ListingModel->prep_firme(),
			'query3' => $this->ListingModel->nove_firme(),
			'query4' => $this->ListingModel->sluc_firme()
			);
		$data['title'] = $data['query']['Ime'];
		$data['opis'] = $data['query']['Opis'];
		if ($data['images']) {
		foreach ($data['images'] as $image) {
			if ($image->prva == 1) {
				$data['image'] = base_url()."uploads/".$image->ime;
			}
		}
	}
	else {
		$data['image'] = base_url()."images/nema-slike.jpg";
	}
		
		
		$this->load->view('listing/item', $data);
	}
        
        function add_listing() {
		$config['ul_class']='treeview';
        $config['table']='categories';
        $this->load->library('tree',$config);
        $data = array(
            'title' => 'Dodaj firmu',
            'tree' => $this->tree->getTree(),
            'slika' => base_url() . "images/logo_default.png",
            'opis' => "Najveća Online baza firmi u BiH te najnovije vijesti iz svijeta biznisa"
        );
        $this->load->library('form_validation');
        $this->load->helper('security');
        $this->form_validation->set_error_delimiters('<div class="alert alert-error">', '</div>');      
        $this->form_validation->set_rules('ime_firme', 'Ime firme', 'trim|required|strip_tags|xss_clean');
        $this->form_validation->set_rules('adresa', 'Adresa', 'trim|required|strip_tags|xss_clean');
        $this->form_validation->set_rules('postanski', 'Postanski broj', 'trim|required|strip_tags|xss_clean');
        $this->form_validation->set_rules('grad', 'Grad', 'trim|required|strip_tags|xss_clean');
        $this->form_validation->set_rules('kanton', 'Kanton', 'trim|required|strip_tags|xss_clean');
        $this->form_validation->set_rules('drzava', 'Drzava', 'trim|required|strip_tags|xss_clean');
        $this->form_validation->set_rules('telefon1', 'Telefon', 'trim|required|strip_tags|xss_clean');
        $this->form_validation->set_rules('email1', 'Email', 'trim|strip_tags|xss_clean|valid_email');

        $forma_zamka = $this->input->post('zamka');
        if (!empty($forma_zamka)) {
        	return false;
        }
        else {


        if ($this->form_validation->run() === FALSE)
            {   
                $this->load->view('listing/AddListing', $data);
                        
            }
        else
            {
            
                
                $config['upload_path'] = './uploads/';
                $config['allowed_types'] = 'gif|jpg|png|jpeg|';
                $config['max_size']    = '1000';
                $config['max_width']  = '1600';
                $config['max_height']  = '1200';
        
                $this->load->library('upload', $config);

                $this->upload->do_multi_upload('userfile');

                $image = $this->upload->get_multi_upload_data('userfile');

                $this->load->library('image_lib');

                foreach ($image as $img) {

                $config['image_library'] = 'gd2';
                $config['source_image'] = './uploads/'.$img['file_name'];
                $config['new_image'] = './uploads/thumbs/'.$img['file_name'];
                $config['maintain_ratio'] = TRUE;
                $config['width'] = 270;
                $config['height'] = 270;

                $this->image_lib->initialize($config);
 
                $this->image_lib->resize();
                $this->image_lib->clear();

                }  

        
                $this->site_model->addItem();
                $id = mysql_insert_id();
                $this->AdminModel->itemsCategories($id);
                $slike = $this->upload->get_multi_upload_data();
                $this->AdminModel->imagesItems($id, $slike);
                $this->load->library('email');
                $config['wordwrap'] = TRUE;
                $config['mailtype'] = 'html';
                $this->email->initialize($config);

                $this->email->from('opibamail@gmail.com', 'Opi.ba Poslovni imenik');
                $this->email->to('opibamail@gmail.com');
                $this->email->subject('Firma potrebna za odobrenje');
                $this->email->message('Poslan je novi zahtjev za dodavanje firme, te je potrebno pregledati podatke i ako je sve uredu odobriti je:<br>
					Ime firme: ' . $this->input->post('ime_firme') . '<br>
					Opis Firme: ' . $this->input->post('opis_firme') . '<br>
					<a href="' . base_url() . 'admin/firme/odobri-firmu/' . $id . '">Pregledaj, uredi firmu i odobri firmu</a>'
                );

                $this->email->send();

                if ($this->input->post('email1')) {
                    $this->email->from('opibamail@gmail.com', 'Opi.ba Poslovni imenik');
                    $this->email->to($this->input->post('email1'));

                    $this->email->subject('Firma uspješno dodana');
                    $this->email->message('Poštovani,<br>

					Vaš zahtjev za dodavanje nove firme u Opi.ba bazu je uspješno poslan. Nakon odobravanja od strane administracije dobit ćete email o potvrdi. <br><br>

					Hvala što koristite Opi.ba
					<br>
					<br>

					S poštovanjem
					<br>
					Opi.ba Tim'
                    );

                    $this->email->send();
                }
                $this->session->set_flashdata('notification', 'Uspješno ste unijeli podatke. Potrebno je još samo da naši Administratori odobre Vaš zahtjev ukoliko su podaci ta�?no uneseni.');
                redirect('site');
            
            }
        }
	}
}