<?php $this->load->view('admin/header'); ?>
<script>
$(document).ready(function() {
     initialize();
})
</script>
<div id="sadrzaj">
	<div id="sadrzaj_block">
	<div id="head_bg">
		<div id="head_txt">Uredi firmu</div>
	</div>
	<div id="lista">
		<div id="line"></div>
<?php echo form_open(); ?>
	<table width="1000">
		<tr>
			<td valign="top">
	<fieldset>
		<legend>Detalji o firmi</legend>
<label>Ime Firme:</label>
<?php echo form_input('ime_firme', $que['Ime']); ?>
<?php echo validation_errors('<div class="error">'); ?>
<div class="tre_kat">
    Trenutna kategorija: 
    <strong><?php echo $podkateg['Ime_kategorije']; ?></strong>
<input type="hidden" name="kategorija" value="<?php echo $podkateg['id'] ?>">
</div>
<div class="dodaj_jos">Promijeni kategoriju</div>
<label>Opis Firme:</label>

<textarea class="tiny" name="opis_firme"><?php echo $que['Opis'] ?></textarea>
<label>Adresa Firme:</label>
<?php echo form_input('adresa', $que['Adresa']); ?>
<label>Poštanski broj:</label>
<?php echo form_input('postanski', $que['Postanski_br']); ?>
<label>Grad:</label>
<?php echo form_input('grad', $que['Grad']); ?>
<label>Kanton:</label>
<?php echo form_input('kanton', $que['Kanton']); ?>
<label>Država:</label>
<?php echo form_input('drzava', $que['Drzava']); ?>
<label>Telefon 1:</label>
<?php echo form_input('telefon1', $que['Telefon1']); ?>
<label>Telefon 2:</label>
<?php echo form_input('telefon2', $que['Telefon2']); ?>
<label>Telefon 3:</label>
<?php echo form_input('telefon3', $que['Telefon3']); ?>
<label>Telefon 4:</label>
<?php echo form_input('telefon4', $que['Telefon4']); ?>
<label>Fax:</label>
<?php echo form_input('fax', $que['Fax']); ?>
<label>Email 1:</label>
<?php echo form_input('email1', $que['Email1']); ?>
<label>Email 2:</label>
<?php echo form_input('email2', $que['Email2']); ?>
<label>Web stranica:</label>
<?php echo form_input('web', $que['webstranica']); ?>
</fieldset>
</td>
<td valign="top" width="350">
<fieldset>
	<legend>Slike</legend>
<div class="well" style="padding: 14px 19px;">
    <input type="file" name="userfile[]">
    <input type="file" name="userfile[]">
    <input type="file" name="userfile[]">
    <input type="file" name="userfile[]">
    <input type="file" name="userfile[]">
    <input type="file" name="userfile[]">
    <input type="file" name="userfile[]">
    <input type="file" name="userfile[]">
    <input type="file" name="userfile[]">
    <input type="file" name="userfile[]">
</div>
<?php if ($slika): ?>
    <label>Slike prethodno dodane:</label>
<?php foreach ($slika as $sl): ?>
<div style="clear: both;"></div>
	<div id="item_img">
        <a id="removeImage" class="obrisi obr_sl" href="#" data-id="<?php echo $sl->id ?>"></a>
		<img src="<?php echo base_url().'/uploads/thumbs/'.$sl->ime ?>"/>
	</div>
<?php endforeach ?>
<?php endif ?>
</fieldset>
<fieldset>
<?php if($que['lat'] && $que['lng'] > 0)
{
    $lat = $que['lat'];
    $lng = $que['lng'];
    $zoom = $que['zoom'];
}
else {
    $lat = 44.21371012604076;
    $lng = 17.753906375000042;
    $zoom = 7;
}?>
<legend>Mapa</legend>
<script type="text/javascript"
      src="http://maps.google.com/maps/api/js?sensor=false">
    </script>
<script type="text/javascript">
  function initialize() {
    var myLatlng = new google.maps.LatLng(<?php echo $lat ?>,<?php echo $lng ?>);
    var mapTypeIds = [];
            for(var type in google.maps.MapTypeId) {
                mapTypeIds.push(google.maps.MapTypeId[type]);
            }
    mapTypeIds.push("OSM");
    var myOptions = {
      zoom: <?php echo $zoom ?>,
      center: myLatlng,
      mapTypeId: "OSM",
      mapTypeControlOptions: {
                    mapTypeIds: mapTypeIds
                }
    }
    var map = new google.maps.Map(document.getElementById("map-canvas"), myOptions);
    
    var marker = new google.maps.Marker({
        position: myLatlng, 
        map: map,
        draggable:true
    });
    google.maps.event.addListener(
        marker,
        'dragend',
        function() {
            document.getElementById('lat').value = marker.position.lat();
            document.getElementById('lng').value = marker.position.lng();
            document.getElementById('zoom').value = map.getZoom();
        }
    );
    marker.setMap(map);           
 
            map.mapTypes.set("OSM", new google.maps.ImageMapType({
                getTileUrl: function(coord, zoom) {
                    return "http://tile.openstreetmap.org/" + zoom + "/" + coord.x + "/" + coord.y + ".png";
                },
                tileSize: new google.maps.Size(256, 256),
                maxZoom: 18
            }));
  }
</script>
<input type="hidden" id="lat" name="lat" value="<?php echo $que['lat'] ?>"/>
<input type="hidden" id="lng" name="lng" value="<?php echo $que['lng'] ?>"/>
<input type="hidden" id="zoom" name="zoom" value="<?php echo $que['zoom'] ?>"/>
<div id="map-canvas"></div>
</fieldset>
</td>
</tr>
</table>
<input type="hidden" name="id" value="<?php echo $que['id'] ?>"/>
<div style="clear: both;"></div>
<?php echo form_submit('submit', $button_text); ?>
<?php echo form_close(); ?>
<?php echo validation_errors('<div class="error">'); ?>
<div style="clear:both;"></div>
   </div>
</div>
<script>
$(document).ready(function() {
    var tree = '<?php echo $tree; ?>';
    $('tre_kat').html();
    $('.dodaj_jos').click(function() {

      $('.tre_kat').html('<label>Kategorija:</label>' + tree + '</select>');
   });
});
</script>

<?php $this->load->view('admin/footer'); ?>