<?php $this->load->view('admin/header'); ?>
<div id="sadrzaj">
	<div id="sadrzaj_block">
	<div id="head_bg">
		<div id="head_txt">Menu</div>
		<div id="add_btn">
			<a href="<?php echo base_url()?>admin/menu/add-menu">Add new menu</a>
		</div>
	</div>
	<div id="lista">
		<div id="naslov">Name</div>
		<div id="line"></div>
		<?php foreach($query as $q) : ?>
			<div id="item">
				<a href="<?php echo base_url();?>admin/menu/edit-menu/<?php echo $q->id ?>"><?php echo $q->naslov; ?></a>
				<a id="removeNewsItem" class="obrisi" href="#" title="obriši" data-id="<?php echo $q->id ?>"></a>
				<div class="autor">Parent menu: <strong><?php echo $q->ime ?></strong></div>
			</div>
		<?php endforeach ?>
		<div style="clear:both;"></div>
	</div>
	<?php echo $link ?>
</div>
<?php $this->load->view('admin/footer'); ?>

