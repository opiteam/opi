﻿<div style="clear:both;"></div>
<div class="footer">
  <div class="container">
    <div class="row">
      <div class="span4">
        <div class="logo"></div>
      </div>
      <div class="span4">
        <h3><i class="icon-list icon-white"></i> Linkovi</h3>
        <ul>
          <li><a href="<?php base_url()?>">PoÄ?etna</a></li>
          <li><a href="<?php base_url()?>poslovne-novosti">Poslovne novosti</a></li>
          <li><a href="<?php base_url()?>poslovni-imenik">Poslovni imenik</a></li>
          <li><a href="<?php base_url()?>dodaj-firmu">Dodaj firmu</a></li>
          <li><a href="<?php base_url()?>opi-marketing">Marketing</a></li>
          <li><a href="<?php base_url()?>kontakt">Kontakt</a></li>
        </ul>
      </div>
      <div class="span4">
        <h3><i class="icon-book icon-white"></i> O nama</h3>
        Opi.ba - Online poslovne informacije je web portal na kojem moÅ¾ete pronaÄ‡i sve potrebne informacije u vezi Å¾eljene firme kao i najnovije vijesti iz svijeta biznisa.
      </div>
    </div>
  </div>
  <div class="prava">
  	<div class="container">
  		&copy; 2012 - <?php echo date('Y') ?> Opi.ba - Online Poslovne Informacije
  	</div>
  </div>
</div>
</div>
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script src="<?=base_url()?>/js/lightbox-2.6.min.js"></script>
    <link href="<?=base_url()?>/css/lightbox.css" rel="stylesheet" />
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800,400italic&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
	  <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
   	<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
	<script type="text/javascript" src="<?=base_url()?>/js/vendor/jquery.flexslider.min.js"></script>
	<script type="text/javascript" src="<?=base_url()?>/js/vendor/jquery.fitvids.min.js"></script>
	<script type="text/javascript" src="<?=base_url()?>/js/vendor/jquery.isotope.min.js"></script>
	<script type="text/javascript" src="<?=base_url()?>/js/vendor/jquery.prettyPhoto.min.js"></script>
	<script type="text/javascript" src="<?=base_url()?>/js/vendor/jquery.gmap3.min.js"></script>
	<script type="text/javascript" src="<?=base_url()?>/js/vendor/jquery.qrcode.min.js"></script>
	<script type="text/javascript" src="<?=base_url()?>/js/vendor/jquery.cookie.min.js"></script>
	<script type="text/javascript" src="<?=base_url()?>/js/vendor/jquery.jcarousel.min.js"></script>
	<script type="text/javascript" src="<?=base_url()?>/js/vendor/jquery.switcher.js"></script>
  <script type="text/javascript" src="<?=base_url()?>/js/vendor/modernizr.min.js"></script>
  <script type="text/javascript" src="<?=base_url()?>/js/vendor/jquery.parallax.min.js"></script>
	<script type="text/javascript" src="<?=base_url()?>/js/plugins.js"></script>
	<script src="<?=base_url()?>/js/jquery.easing.1.3.js"></script>
  	<script src="<?=base_url()?>/js/jquery.touchSwipe.min.js"></script>
  	<script src="<?=base_url()?>/js/jquery.liquid-slider-custom.min.js"></script>  
  	 <script type="text/javascript" src="<?=base_url()?>/js/tinymce/tinymce.min.js"></script>
     <script src="<?=base_url()?>/js/main.js"></script>
     <script>
$(document).ready(function() {
  if ($('.navigation').length > 0) {
    var top = $('.navigation').offset().top;
  }

      var width = $(window).width();

      if (Modernizr.mq('screen and (min-width: 980px)')) {
        var scroll_top = $(window).scrollTop();

        if (scroll_top > top) {
          $('nav').css({
            'position': 'fixed',
            'top': 0,
            'left': 0
          });

          $('.boxed nav').css({
            'left': (width / 2 - $('.boxed nav').width() / 2) + 'px'
          });
        } else {
          $('nav').css({
            'position': 'relative',
            'left': 0
          });
        }
      }

        $('#nav .menu').tinyNav({
          header: 'Idi na...'
        });
  
})
</script>


  	<script>
    $('#main-slider').liquidSlider({
  		crossLinks:true,
  		hashCrossLinks: true,

    });
  </script>
  <script type="text/javascript">
tinymce.init({
    selector: "textarea.tiny",
    theme: "modern",
    width: 640,
    plugins: [
        "advlist autolink lists link image charmap print preview anchor",
        "searchreplace visualblocks code fullscreen",
        "insertdatetime media table contextmenu paste filemanager"
    ],
    image_advtab: true,

    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
});
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-41238295-1', 'opi.ba');
  ga('send', 'pageview');

</script>
</body>
</html>
