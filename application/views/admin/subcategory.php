<?php $this->load->view('admin/header'); ?>
<div id="sadrzaj">
	<div id="sadrzaj_block">
	<div id="head_bg">
		<?php if ($podkat): ?>
		<?php foreach ($podkat as $k): ?>
    		<div id="head_txt">Listings in <?php echo $k->categoryName ?> subcategory</div>
    	<?php endforeach ?>
    	<?php else: ?>
    		<div id="head_txt">All Listings per date added</div>
    	<?php endif ?>
      	<div id="add_btn">
			<a href="<?php echo base_url('admin/listing/addItem/')?><?php echo $this->uri->segment(4)?>">Add Listing</a>
		</div>
	</div>
	<div id="lista">
		<div id="naslov">Listing Name</div>
		<div id="line"></div>
<?php foreach ($query as $item): ?>
	<?php 
		$url = base_url();
		$ndatum = $item->date;
		$datum = date("d.m.Y", strtotime($ndatum));

		if ($item->approved == 0) {
			echo <<<html
			<div id="item" class="isteklo">
				<a href="{$url}admin/listing/approveItem/{$item->id}">{$item->name}</a>

				<a id="removeItem" class="obrisi" href="#" title="obriši" data-id="{$item->id}"></a>

				<div class="autor raz">Created: <strong>{$datum}</strong></div>

				<div class="autor raz">Added by: <strong>{$item->username}</strong></div>
			</div>
html;

		}
		else {

			echo <<<html
			<div id="item">
				<a href="{$url}admin/listing/editItem/{$item->id}">{$item->name}</a>

				<a id="removeItem" class="obrisi" href="#" title="obriši" data-id="{$item->id}"></a>

				<div class="autor raz">Created: <strong>{$datum}</strong></div>

				<div class="autor raz">Added by: <strong>{$item->username}</strong></div>
			</div>
html;
		}?>
<?php endforeach ?>
	</div>
<div style="clear:both;"></div>
<?php echo $link; ?>
</div>

<?php $this->load->view('admin/footer'); ?>