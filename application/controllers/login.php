<?php
class Login extends CI_Controller {

	
	
   function index()
   {
       
	  if (!$this->session->userdata('logged_in'))
	  {
  
          $this->load->view('admin/login_form');
		  
	  }
	  
	  else
	  {
		  redirect('admin');
		  
	  }
   
   }
   
   function validation()
   {

   $this->load->library('form_validation');
   $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
   $this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean');
   $this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean|callback_check_database');

   if($this->form_validation->run() == FALSE)
   {
    
     $this->load->view('admin/login_form');
   
   }
   
   else
   
   {
      if ($this->session->userdata('last_page'))
      {
        redirect($this->session->userdata('last_page'));
      }
      else {
        redirect('admin/dashboard');
      }
   }

 }

 function check_database($password)
 {
   
   $username = $this->input->post('username');

   
   $result = $this->AdminModel->login($username, $password);

   if($result)
   {
     $data = array();
     foreach($result as $row)
     {
       $data = array(
         'id' => $row->id,
         'username' => $row->username,
         'tip' => $row->type,
		     'logged_in' => true
		 
		 
       );
       $this->session->set_userdata($data);
     }
     return TRUE;
   }
   else
   {
     $this->form_validation->set_message('check_database', 'Netačan username ili password');
     return false;
   }
 }
     
}



