<?php

class Dashboard extends CI_Controller {

    function __construct()
	{
		
		parent::__construct();
		$this->logged();
		
    }
	
    
    function index()
	{
                $data = array(
                    'title' => 'Opi.ba',
                    'username' => $this->session->userdata['username'],
                    'itemsCount' => $this->AdminModel->itemsCount(),
                    'users' => $this->AdminModel->users(),
                    'lastItems' => $this->AdminModel->lastItems(),
                    'lastNews' => $this->AdminModel->lastNews()
                ); 
		$this->load->view('admin/home', $data);
	} 
 
    protected function logged(){
	
	$is_logged_in = $this->session->userdata('logged_in');
	
	if(!isset($is_logged_in) || $is_logged_in != true)
	
	{
        $this->load->helper('url');
        $this->session->set_userdata('last_page', current_url());
		redirect('login');
		
	}
	
    }
	
	function logout()
	{
		$this->session->unset_userdata('logged_in');
        redirect('login', 'refresh');
	}
}