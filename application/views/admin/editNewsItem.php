<?php $this->load->view('admin/header'); ?>
<div id="sadrzaj">
	<div id="sadrzaj_block">
	<div id="head_bg">
		<div id="head_txt">Dodaj Novu Firmu</div>
	</div>
	<div id="lista">
		<div id="line"></div>

<?php echo form_open('admin/editNewsItem'); ?>
<?php echo validation_errors('<div class="error">'); ?>
<table width="600">
		<tr>
			<td valign="top">
	<fieldset>
		<legend>Detalji o vijesti</legend>
<label>Naslov:</label>
<?php foreach ($query as $q): ?>
<?php echo form_input('naslov', $q->naslov); ?>
    <label>Kategorija:</label>
    <select name="kategorija">
        <option value="<?php echo $q->id_kategorije ?>" selected="selected"><?php echo $q->ime ?></option>
    <?php foreach ($kateg as $kat){
    
    $kat_id = $kat->id;
    $kat_ime = $kat->ime;
    echo <<<html
    <option value="{$kat_id}">{$kat_ime}</option>

html;
}?>
    </select>
<label>Sadržaj:</label>
<textarea class="tiny" name="sadrzaj"><?php echo $q->sadrzaj ?></textarea>
<input type="hidden" name="id" value="<?php echo $q->id ?>" />
<?php endforeach ?>
</fieldset>
</td>
</td>
</tr>
</table>
<?php echo form_submit('submit', 'Uredi vijest'); ?>
<?php echo form_close(); ?>
<div style="clear:both;"></div>
</div>
</div>
<?php $this->load->view('admin/footer'); ?>