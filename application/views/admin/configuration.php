<?php $this->load->view('admin/header'); ?>
<div id="sadrzaj">
	<div id="sadrzaj_block">
		<div id="head_bg">
        	<div id="head_txt">Configuration</div>
    	</div> 
		<div id="lista">
			<div id="line"></div>
			<?php echo form_open_multipart('admin/configuration'); ?>
			<?php echo validation_errors('<div class="error">'); ?>
			<?php if( isset( $error) ) echo $error; ?>
			<table width="100%">
			<tr>
			<td valign="top">
			<fieldset>
				<legend>Website information</legend>
			<label>Website name:</label>
			<?php echo form_input('site_name', $query['SiteName']); ?>
			<label>Website description:</label>
			<?php echo form_input('site_desc', $query['SiteDesc']); ?>
			<label>Logo:</label>
			<?php echo form_upload('userfile'); ?>
			<input type="hidden" name="userfile" value="<?php echo $query['Logo'] ?>"/>
			<div style="clear: both;"></div>
			<label>Current Logo</label>
			<?php if ($query['Logo']): ?>
				<img src="<?php echo base_url().'images/'.$query['Logo']?>" />
			<?php endif; ?>
			</fieldset>
			</td>
			<td valign="top">
			<fieldset>
				<legend>Basic Settings</legend>
				<label>Show published on in articles</label>
				<label class="radio-inline">
				<?php if ($query['ShowPublished'] == 1):?>
					<input type="radio" name="show_published" value="1" checked="checked"> Yes
					<input type="radio" name="show_published" value="0"> No
				<?php else: ?>
					<input type="radio" name="show_published" value="1"> Yes
					<input type="radio" name="show_published" value="0" checked="checked"> No
				<?php endif; ?>
				</label>
				<label>Show category</label>
				<label class="radio-inline">
					<input type="radio" name="show_category" value="1" checked="checked"> Yes
					<input type="radio" name="show_category" value="0"> No
				</label>
			</fieldset>
			</td>
			</tr>
			</table>
			<?php echo form_submit('submit', 'Save'); ?>
			<?php echo form_close(); ?>
			<div style="clear:both"></div>
		</div>
	</div>

<?php $this->load->view('admin/footer'); ?>