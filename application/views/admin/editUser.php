<?php $this->load->view('admin/header'); ?>
<div id="sadrzaj">
	<div id="sadrzaj_block">
	<div id="head_bg">
		<div id="head_txt">Uredi korisnika</div>
	</div>
	<div id="lista">
		<div id="line"></div>
<?php echo form_open('admin/uredi_korisnika'); ?>
<?php echo validation_errors('<div class="error">'); ?>
<table width="600">
		<tr>
			<td valign="top">
	<fieldset>
		<legend>Detalji o korisniku</legend>
<label>Korisničko ime:</label>
<?php foreach ($query as $q): ?>
<?php echo form_input('ime', $q->username); ?>
<label>Tip korisnika:</label>
        <select name="tip">
                    <?php switch ($q->type) {
                        case 1:
                            echo '<option selected="selected" value="1">Administrator</option>';
                            echo '<option value="2">Korisnik</option>';
                            break;
                        
                        case 2:
                            echo '<option selected="selected" value="2">Korisnik</option>';
                            echo '<option value="1">Administrator</option>';
                            break;
                    }?>
                </select>
<label>Šifra:</label>
<?php echo form_password('password'); ?>
<input type="hidden" name="id" value="<?php echo $q->id ?>" />
<?php endforeach ?>
<label>Potvrdi šifru:</label>
<?php echo form_password('password_potvrda'); ?>
</fieldset>
</td>
</td>
</tr>
</table>
<?php echo form_submit('submit', 'Uredi korisnika'); ?>
<?php echo form_close(); ?>
<div style="clear:both;"></div>
</div>
</div>
<?php $this->load->view('admin/footer'); ?>