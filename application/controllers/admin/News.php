<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class News extends CI_Controller {
        function __construct()
	{	
            parent::__construct();
		
            $session_data = $this->session->userdata('logged_in');
            $data['username'] = $session_data['username'];	
	}
        
	public function index()
	{
         $this->load->library("pagination");
		$config['per_page'] = 10;
		$config["base_url"] = base_url()."poslovne-novosti";
		$config['uri_segment'] = 2;
		$config['num_links'] = 9;
		$config['total_rows'] = $this->AdminModel->NewsCount();
		$config['full_tag_open'] = '<div class="pagination"><ul>';
		$config['full_tag_close'] = '</ul></div><!--pagination-->';
		 
		$config['first_link'] = '&laquo; Prva';
		$config['first_tag_open'] = '<li class="prev page">';
		$config['first_tag_close'] = '</li>';
		 
		$config['last_link'] = 'Zadnja &raquo;';
		$config['last_tag_open'] = '<li class="next page">';
		$config['last_tag_close'] = '</li>';
		 
		$config['next_link'] = 'Slijedeća &rarr;';
		$config['next_tag_open'] = '<li class="next page">';
		$config['next_tag_close'] = '</li>';
		 
		$config['prev_link'] = '&larr; Prethodna';
		$config['prev_tag_open'] = '<li class="prev page">';
		$config['prev_tag_close'] = '</li>';
		 
		$config['cur_tag_open'] = '<li class="active"><a href="">';
		$config['cur_tag_close'] = '</a></li>';
		 
		$config['num_tag_open'] = '<li class="page">';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
		$data = array(
			'query' => $this->site_model->news($config["per_page"], $page),
			'opis' => "Najveća Online baza firmi u BiH te najnovije vijesti iz svijeta biznisa",
			'slika' => base_url()."images/logo_default.png",
			'title' => 'Poslovne novosti',
			'link' => $this->pagination->create_links(),
			'query2' => $this->site_model->vijesti_kategorije_meni()
			);
            $this->load->view('header');
            $this->load->view('news/index', $data);
            $this->load->view('footer');
	}
        
        function item($link) {
		$link = $this->uri->segment(3);
		
		$data = array(
			'query' => $this->site_model->newsItem($link),
			'query2' => $this->site_model->vijesti_kategorije_meni()
			);
		$pic = $data['query']['sadrzaj'];
 		preg_match_all('/src=[\'"]?([^\'" >]+)[\'" >]/', $pic, $output, PREG_PATTERN_ORDER); 
		foreach ($output as $out) {
			$data['slika'] = $out[0];
		}										
		$data['title'] = $data['query']['naslov'];
		$data['opis'] = $data['query']['sadrzaj'];
		$this->load->view('site/vijest', $data);
	}
        
        function category($link) {
		$this->load->library("pagination");
		$config['per_page'] = 10;
		$config["base_url"] = base_url()."poslovne-novosti/".$link;
		$config['uri_segment'] = 3;
		$config['num_links'] = 9;
		$config['total_rows'] = $this->site_model->NewsPerCategoryCount($link);
		$config['full_tag_open'] = '<div class="pagination"><ul>';
		$config['full_tag_close'] = '</ul></div><!--pagination-->';
		 
		$config['first_link'] = '&laquo; Prva';
		$config['first_tag_open'] = '<li class="prev page">';
		$config['first_tag_close'] = '</li>';
		 
		$config['last_link'] = 'Zadnja &raquo;';
		$config['last_tag_open'] = '<li class="next page">';
		$config['last_tag_close'] = '</li>';
		 
		$config['next_link'] = 'Slijedeća &rarr;';
		$config['next_tag_open'] = '<li class="next page">';
		$config['next_tag_close'] = '</li>';
		 
		$config['prev_link'] = '&larr; Prethodna';
		$config['prev_tag_open'] = '<li class="prev page">';
		$config['prev_tag_close'] = '</li>';
		 
		$config['cur_tag_open'] = '<li class="active"><a href="">';
		$config['cur_tag_close'] = '</a></li>';
		 
		$config['num_tag_open'] = '<li class="page">';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$data = array(
			'query' => $this->site_model->vijesti_kategorija($link, $config["per_page"], $page),
			'slika' => base_url()."images/logo_default.png",
			'link' => $this->pagination->create_links(),
			'title' => 'Poslovne novosti',
			'opis' => "Najveća Online baza firmi u BiH te najnovije vijesti iz svijeta biznisa",
			'query2' => $this->site_model->vijesti_kategorije_meni()
			);

		$this->load->view('site/vijesti', $data);
	}
        
        function addCategory() {
        $data = array(
            'title' => "New Category"
            );
        $this->load->library('form_validation');
        $this->load->helper('security');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');        
        $this->form_validation->set_rules('ime_kategorije', 'Ime kategorije', 'trim|required|strip_tags|xss_clean');


        if ($this->form_validation->run() === FALSE)
            {       
                $this->load->view('admin/dodaj_vijestikat', $data);
                        
            }
        else
            {

                $this->admin_model->dodaj_vijestikat();
                $id = mysql_insert_id();
                $this->session->set_flashdata('notification', 'Kategorija je dodana');
                redirect('admin/vijesti/uredi-kategoriju/'.$id);
                
            }
    }

    function editCategory($id) {
        $data = array(
            'title' => "Edit Category",
            'query' => $this->admin_model->kategorija_za_vijesti($id)
            );
        $this->load->library('form_validation');
        $this->load->helper('security');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');        
        $this->form_validation->set_rules('ime_kategorije', 'Ime kategorije', 'trim|required|strip_tags|xss_clean');


        if ($this->form_validation->run() === FALSE)
            {       
                $this->load->view('admin/uredi_vijestikat', $data);
                        
            }
        else
            {

                $this->admin_model->uredi_vijestikat();
                $this->session->set_flashdata('notification', 'Vijest uspješno uređena');
                redirect('admin/vijesti_kategorije');
                
            }
    }

    function addItem() {
        $data = array(
            'title' => "New Article",
            'kateg' => $this->admin_model->vijestikat()
            );
        $this->load->library('form_validation');
        $this->load->helper('security');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');       
        $this->form_validation->set_rules('naslov', 'Naslov', 'trim|required|strip_tags|xss_clean');


        if ($this->form_validation->run() === FALSE)
            {       
                $this->load->view('admin/dodaj_vijest', $data);
                        
            }
        else
            {

                $this->admin_model->dodaj_vijest();
                $id = mysql_insert_id();
                $this->session->set_flashdata('notification', 'Article published');
                redirect('admin/vijesti/uredi-vijest/'.$id);
                
            }
    }

    function editItem($idv) {
        $this->session->set_userdata('last_page_vij', current_url());
        $data = array(
            'title' => "Edit Article",
            'kateg' => $this->admin_model->vijestikat(),
            'query' => $this->admin_model->vijest($idv)
            );
        $this->load->library('form_validation');
        $this->load->helper('security');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');        
        $this->form_validation->set_rules('naslov', 'Naslov', 'trim|required|strip_tags|xss_clean');


        if ($this->form_validation->run() === FALSE)
            {       
                $this->load->view('admin/uredi_vijest', $data);
                        
            }
        else
            {
                
                $this->admin_model->uredi_vijest();
                $this->session->set_flashdata('notification', 'Vijest uspješno uređena');
                redirect($this->session->userdata('last_page_vij'));
                
            }
    }
}