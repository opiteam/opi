<?php $this->load->view('admin/header'); ?>
<div id="sadrzaj">
	<div id="sadrzaj_block">
	<div id="head_bg">
		<div id="head_txt">Add User</div>
	</div>
	<div id="lista">
		<div id="line"></div>

<?php echo form_open('admin/users/create'); ?>
<?php echo validation_errors('<div class="error">'); ?>
<table width="600">
		<tr>
			<td valign="top">
	<fieldset>
		<legend>User Details</legend>
<label>Username:</label>
<?php echo form_input('name'); ?>
<label>User Type:</label>
        <select name="type">
            <option value="1">Administrator</option>
            <option value="2">User</option>
        </select>
<label>Password:</label>
<?php echo form_password('password'); ?>
<label>Confirm Password:</label>
<?php echo form_password('password_confirm'); ?>
</fieldset>
</td>
</td>
</tr>
</table>
<?php echo form_submit('submit', 'Create User'); ?>
<?php echo form_close(); ?>
<div style="clear:both;"></div>
</div>
</div>
<?php $this->load->view('admin/footer'); ?>