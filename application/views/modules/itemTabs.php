<aside>
	<div class="widget">
		<ul class="nav nav-tabs" id="myTab">
  			<li class="active"><a href="#izdvojeno"><i class="icon-flag"></i> Izdvojeno</a></li>
  			<li><a href="#novo"><i class="icon-time"></i> Novo</a></li>
  			<li><a href="#slucajno"><i class="icon-random"></i> Slučajno</a></li>
		</ul>
		<div class="tab-content">
  			<div class="tab-pane active" id="izdvojeno">
  				<ul class="tabovi">
            <?php foreach ($query2 as $q): ?>
            <?php $id_nosece = $q->id_nosece;
            $id = $q->id;
            $slika = $this->site_model->nove_firme_slike($id);
            $brojac = $this->site_model->nove_firme_posjete($id);
            $link_kat = $this->site_model->nove_firme_kat($id_nosece);
            ?>
  					<li class="tab-firme">
              <a href="<?php echo base_url().'poslovni-imenik/item/'.$q->id.'/'.$q->link ?>">
                <?php if($slika): ?>
                <img src="<?php echo base_url().'uploads/thumbs/'.$slika['ime']; ?>" />
                <?php else: ?>
                <img src="<?php echo base_url() ?>images/nema-slike.jpg" />
                <?php endif ?>
              </a>
              <h5><a href="<?php echo base_url().'poslovni-imenik/item/'.$q->id.'/'.$q->link ?>">
                <?php echo $q->Ime; ?>
              </a>
              <?php if($brojac): ?>
              <small class="pull-right"><?php echo $brojac['brojac'];?> <i class="icon-eye-open"></i></small>
              <?php else: ?>
              <small class="pull-right">0 <i class="icon-eye-open"></i></small>
             <?php endif ?>
              </h5>
              <small>Kategorija: <a href="<?php echo base_url().'poslovni-imenik/'.$link_kat['link_kategorije'].'/'.$q->link_kategorije ?> "><?php echo $q->Ime_kategorije; ?></a></small>
  					</li>
          <?php endforeach ?>
  				</ul>
  			</div>
 			<div class="tab-pane" id="novo">
        <ul class="tabovi">
            <?php foreach ($query3 as $q): ?>
            <?php $id_nosece = $q->id_nosece;
            $id = $q->id;
            $slika = $this->site_model->nove_firme_slike($id);
            $brojac = $this->site_model->nove_firme_posjete($id);
            $link_kat = $this->site_model->nove_firme_kat($id_nosece);
            ?>
            <li class="tab-firme">
              <a href="<?php echo base_url().'poslovni-imenik/'.$link_kat['link_kategorije'].'/'.$q->link_kategorije.'/'.$q->link ?>">
                <?php if($slika): ?>
                <img src="<?php echo base_url().'uploads/thumbs/'.$slika['ime']; ?>" />
                <?php else: ?>
                <img src="<?php echo base_url() ?>images/nema-slike.jpg" />
                <?php endif ?>
              </a>
              <h5><a href="<?php echo base_url().'poslovni-imenik/'.$link_kat['link_kategorije'].'/'.$q->link_kategorije.'/'.$q->link ?>">
                <?php echo $q->Ime; ?>
              </a>
              <?php if($brojac): ?>
              <small class="pull-right"><?php echo $brojac['brojac'];?> <i class="icon-eye-open"></i></small>
              <?php else: ?>
              <small class="pull-right">0 <i class="icon-eye-open"></i></small>
             <?php endif ?>
              </h5>
              <small>Dodano: <?php echo date('d.m.Y', strtotime($q->datum)) ?></small>
            </li>
          <?php endforeach ?>
          </ul>
      </div>
  			<div class="tab-pane" id="slucajno">
          <ul class="tabovi">
            <?php foreach ($query4 as $q): ?>
            <?php $id_nosece = $q->id_nosece;
            $id = $q->id;
            $slika = $this->site_model->nove_firme_slike($id);
            $brojac = $this->site_model->nove_firme_posjete($id);
            $link_kat = $this->site_model->nove_firme_kat($id_nosece);
            ?>
            <li class="tab-firme">
              <a href="<?php echo base_url().'poslovni-imenik/'.$link_kat['link_kategorije'].'/'.$q->link_kategorije.'/'.$q->link ?>">
                <?php if($slika): ?>
                <img src="<?php echo base_url().'uploads/thumbs/'.$slika['ime']; ?>" />
                <?php else: ?>
                <img src="<?php echo base_url() ?>images/nema-slike.jpg" />
                <?php endif ?>
              </a>
              <h5><a href="<?php echo base_url().'poslovni-imenik/'.$link_kat['link_kategorije'].'/'.$q->link_kategorije.'/'.$q->link ?>">
                <?php echo $q->Ime; ?>
              </a>
              <?php if($brojac): ?>
              <small class="pull-right"><?php echo $brojac['brojac'];?> <i class="icon-eye-open"></i></small>
              <?php else: ?>
              <small class="pull-right">0 <i class="icon-eye-open"></i></small>
             <?php endif ?>
              </h5>
              <small>Kategorija: <a href="<?php echo base_url().'poslovni-imenik/'.$link_kat['link_kategorije'].'/'.$q->link_kategorije ?> "><?php echo $q->Ime_kategorije; ?></a></small>
            </li>
          <?php endforeach ?>
          </ul>
        </div>
		</div>
	</div>
</aside>
<script>
$('#myTab a').click(function (e) {
  e.preventDefault();
  $(this).tab('show');
})
</script>
