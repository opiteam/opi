<?php $this->load->view('site/header'); ?>
<div class="main">
	<div class="container">
		<div class="row">
			<div class="span12">
				<div class="content">
					<div class="page-header">
						<h1>Kontakt</h1>
					</div>
					<div class="kontakt">
						<div class="span9">				
						<?php echo form_open_multipart('site/kontakt'); ?>
						<?php echo validation_errors('<div class="error">'); ?>
						  <fieldset>

						<label>Za sve informacije možete nas kontaktirati tako što ćete detaljno ispuniti formu ispod.:</label>
						<div class="row-fluid">
						<div class="span6">
						<label>Vaše ime *:</label>
						<?php echo form_input('ime'); ?>
						</div>
						<div class="span6">
						<label>Vaš E-mail *:</label>
						<?php echo form_input('email'); ?>
						</div>
						</div>
						<div class="row-fluid">
						<div class="span6">
						<?php $options = array(
						                  'informacije'  => 'Informacije',
						                  'izmjena_podataka'    => 'Izmjena podataka',
						                  'marketing'   => 'Marketing',
						                );
						?>
						<label>Pitanje u vezi *:</label>
						<?php echo form_dropdown('pitanje', $options, 'informacije'); ?>
						</div>
						<div class="span6">
						<label>Naslov *:</label>
						<?php echo form_input('naslov'); ?>
						</div>
						</div>
						<div class="row-fluid">
						<div class="span12">
						<label>Poruka *:</label>
						<?php $data = array(
						              'name'        => 'poruka',
						              'style'       => 'width:100%',
						            );
						?>
						<?php echo form_textarea($data); ?>
						</div>
						</div>
						</fieldset>
						<fieldset>
						<button type="submit" id="submit" class="btn"><i class="icon-envelope"></i> Pošaljite poruku</button>
						<?php echo form_close(); ?>
						</div>
					</div>
					<div style="clear:both;"></div>
				</div>
			</div>
		</div>
	</div>
<?php $this->load->view('site/footer'); ?>