<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class News extends CI_Controller {
        function __construct()
	{	
            parent::__construct();
		
            $session_data = $this->session->userdata('logged_in');
            $data['username'] = $session_data['username'];	
	}
        
	public function index()
	{
         $this->load->library("pagination");
		$config['per_page'] = 10;
		$config["base_url"] = base_url()."poslovne-novosti";
		$config['uri_segment'] = 2;
		$config['num_links'] = 9;
		$config['total_rows'] = $this->AdminModel->NewsCount();
		$config['full_tag_open'] = '<div class="pagination"><ul>';
		$config['full_tag_close'] = '</ul></div><!--pagination-->';
		 
		$config['first_link'] = '&laquo; Prva';
		$config['first_tag_open'] = '<li class="prev page">';
		$config['first_tag_close'] = '</li>';
		 
		$config['last_link'] = 'Zadnja &raquo;';
		$config['last_tag_open'] = '<li class="next page">';
		$config['last_tag_close'] = '</li>';
		 
		$config['next_link'] = 'Slijedeća &rarr;';
		$config['next_tag_open'] = '<li class="next page">';
		$config['next_tag_close'] = '</li>';
		 
		$config['prev_link'] = '&larr; Prethodna';
		$config['prev_tag_open'] = '<li class="prev page">';
		$config['prev_tag_close'] = '</li>';
		 
		$config['cur_tag_open'] = '<li class="active"><a href="">';
		$config['cur_tag_close'] = '</a></li>';
		 
		$config['num_tag_open'] = '<li class="page">';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
		$data = array(
			'query' => $this->NewsModel->news($config["per_page"], $page),
			'opis' => "Najveća Online baza firmi u BiH te najnovije vijesti iz svijeta biznisa",
			'image' => base_url()."images/logo_default.png",
			'title' => 'Poslovne novosti',
			'link' => $this->pagination->create_links(),
			'query2' => $this->NewsModel->CategoryMenu()
			);
                $this->load->view('news/index', $data);
	}
        
        function item($link) {
		$link = $this->uri->segment(3);
		
		$data = array(
			'query' => $this->site_model->newsItem($link),
			'query2' => $this->site_model->vijesti_kategorije_meni()
			);
		$pic = $data['query']['sadrzaj'];
 		preg_match_all('/src=[\'"]?([^\'" >]+)[\'" >]/', $pic, $output, PREG_PATTERN_ORDER); 
		foreach ($output as $out) {
			$data['slika'] = $out[0];
		}										
		$data['title'] = $data['query']['naslov'];
		$data['opis'] = $data['query']['sadrzaj'];
		$this->load->view('site/vijest', $data);
	}
        
        function category($link) {
		$this->load->library("pagination");
		$config['per_page'] = 10;
		$config["base_url"] = base_url()."poslovne-novosti/".$link;
		$config['uri_segment'] = 3;
		$config['num_links'] = 9;
		$config['total_rows'] = $this->site_model->NewsPerCategoryCount($link);
		$config['full_tag_open'] = '<div class="pagination"><ul>';
		$config['full_tag_close'] = '</ul></div><!--pagination-->';
		 
		$config['first_link'] = '&laquo; Prva';
		$config['first_tag_open'] = '<li class="prev page">';
		$config['first_tag_close'] = '</li>';
		 
		$config['last_link'] = 'Zadnja &raquo;';
		$config['last_tag_open'] = '<li class="next page">';
		$config['last_tag_close'] = '</li>';
		 
		$config['next_link'] = 'Slijedeća &rarr;';
		$config['next_tag_open'] = '<li class="next page">';
		$config['next_tag_close'] = '</li>';
		 
		$config['prev_link'] = '&larr; Prethodna';
		$config['prev_tag_open'] = '<li class="prev page">';
		$config['prev_tag_close'] = '</li>';
		 
		$config['cur_tag_open'] = '<li class="active"><a href="">';
		$config['cur_tag_close'] = '</a></li>';
		 
		$config['num_tag_open'] = '<li class="page">';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$data = array(
			'query' => $this->site_model->vijesti_kategorija($link, $config["per_page"], $page),
			'slika' => base_url()."images/logo_default.png",
			'link' => $this->pagination->create_links(),
			'title' => 'Poslovne novosti',
			'opis' => "Najveća Online baza firmi u BiH te najnovije vijesti iz svijeta biznisa",
			'query2' => $this->site_model->vijesti_kategorije_meni()
			);

		$this->load->view('site/vijesti', $data);
	}
}