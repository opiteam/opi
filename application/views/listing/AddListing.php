<?php $this->load->view('site/header'); ?>
<style>
.required input {
   padding-right: 25px;
   background-image: #ccc;
   background-position: right top;
}
</style>
<div class="main">
	<div class="container">
		<div class="row">
    <div class="kontakt">
			<div class="span12">
				<div class="content">
					<div class="page-header">
						<h1>Dodajte firmu</h1>
					</div>
				
      
        <div class="span6">
            

            <label>Molimo Vas da detaljno i tačno popunite podatke u formu ispod kako bi naš tim mogao pregledati i odobriti</label>
<?php echo form_open_multipart('site/addItem'); ?>
<?php echo validation_errors('<div class="alert alert-error">'); ?>
  <fieldset>
    <legend>Detalji o firmi</legend>
    <label>Sva polja označena sa (*) su obavezna</label>
<label>Ime Firme *:</label>
<?php echo form_input('ime_firme', set_value('ime_firme')); ?>
<?php
    
    if ($this->uri->segment(4)) {
        $id_kat = $this->uri->segment(4);
    echo '<input type="hidden" name="kategorija" value="'.$id_kat.'"/>';

}

else {
    echo '<label>Kategorija *:</label>';
    echo $tree;
    echo '</select>';
} ?>
<label>Opis Firme *:</label>
<textarea name="opis_firme" style="width: 100%; height: 200px;"><?php echo set_value('opis_firme') ?></textarea>
<label>Adresa Firme *:</label>
<?php echo form_input('adresa', set_value('adresa')); ?>
<label>Poštanski broj *:</label>
<?php echo form_input('postanski', set_value('postanski')); ?>
<label>Grad *:</label>
<?php echo form_input('grad', set_value('grad')); ?>
<label>Kanton *:</label>
<?php echo form_input('kanton', set_value('kanton')); ?>
<label>Država *:</label>
<?php echo form_input('drzava', set_value('drzava')); ?>
<label>Telefon 1 *:</label>
<?php echo form_input('telefon1', set_value('telefon1')); ?>
<label>Telefon 2:</label>
<?php echo form_input('telefon2', set_value('telefon2')); ?>
<label>Telefon 3:</label>
<?php echo form_input('telefon3', set_value('telefon3')); ?>
<label>Telefon 4:</label>
<?php echo form_input('telefon4', set_value('telefon4')); ?>
<label>Fax:</label>
<?php echo form_input('fax', set_value('fax')); ?>
<label>Email 1:</label>
<?php echo form_input('email1', set_value('email1')); ?>
<label>Email 2:</label>
<?php echo form_input('email2', set_value('email2')); ?>
<label>Web stranica: (www.primjer.ba)</label>
<?php echo form_input('web', set_value('web')); ?>
<input type="hidden" name="zamka" />
</fieldset>
</td>
</div>
<div class="span4">
<fieldset>
<legend>Slike</legend>
<div class="well" style="padding: 14px 19px;">
    <input type="file" name="userfile[]">
    <input type="file" name="userfile[]">
    <input type="file" name="userfile[]">
    <input type="file" name="userfile[]">
    <input type="file" name="userfile[]">
    <input type="file" name="userfile[]">
    <input type="file" name="userfile[]">
    <input type="file" name="userfile[]">
    <input type="file" name="userfile[]">
    <input type="file" name="userfile[]">
</div>
</fieldset>
<fieldset>
<legend>Mapa</legend>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script>
var geocoder = new google.maps.Geocoder();

function geocodePosition(pos) {
  geocoder.geocode({
    latLng: pos
  });
}


function updateMarkerPosition(latLng) {
  $('#lat').val(latLng.lat());
  $('#lng').val(latLng.lng());
 
}


function initialize() {
  var latLng = new google.maps.LatLng(44.21371012604076,17.753906375000042);
  var map = new google.maps.Map(document.getElementById('map-canvas'), {
    zoom: 7,
    center: latLng,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  });
  var marker = new google.maps.Marker({
    position: latLng,
    title: 'Point A',
    map: map,
    draggable: true
  });
  
  // Update current position info.
  updateMarkerPosition(latLng);
  geocodePosition(latLng);
  
  // Add dragging event listeners.

  
  google.maps.event.addListener(marker, 'dragend', function(evt) {
    $('#lat').val(evt.latLng.lat());
    $('#lng').val(evt.latLng.lng());
    $('#zoom').val(map.getZoom());
  });
}

// Onload handler to fire off the app.
google.maps.event.addDomListener(window, 'load', initialize);

    </script>

<input type="hidden" id="lat" name="lat" value="<?php echo set_value('lat')?>" />
<input type="hidden" id="lng" name="lng" value="<?php echo set_value('lng')?>"/>
<input type="hidden" id="zoom" name="zoom" value="<?php echo set_value('zoom')?>"/>
<div id="map-canvas"></div>
</div>
<div style="clear:both;"></div>
<button type="submit" id="submit" class="btn"><i class="icon-plus"></i> Pošaljite zahtjev</button>
<?php echo form_close(); ?>
<div style="clear:both;"></div>
				</div>
        </div>
			</div>
		</div>
	</div>
<?php $this->load->view('site/footer'); ?>
 



	
