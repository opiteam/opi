<?php $this->load->view('admin/header'); ?>
<div id="sadrzaj">
	<div id="sadrzaj_block">
		<div id="head_bg">
			<?php foreach ($query as $q): ?>
        	<div id="head_txt">Uredi Podkategoriju <?php echo $q->Ime ?></div>
    	</div> 
		<div id="lista">
			<div id="line"></div>
			<?php echo form_open('admin/editSubcategory'); ?>
			<table width="600">
			<tr>
			<td valign="top">
			<fieldset>
				<legend>Podaci o podkategoriji</legend>
			<label>Ime Kategorije:</label>
			<?php echo form_input('ime_podkategorije', $q->Ime); ?>
			</fieldset>
			</td>
			</tr>
			</table>
			<input type="hidden" name="id" value="<?php echo $q->id ?>"/>
			<?php endforeach ?>
			<?php echo form_submit('submit', 'Uredi'); ?>
			<?php echo form_close(); ?>
			<div style="clear:both"></div>
		</div>
	</div>

<?php $this->load->view('admin/footer'); ?>